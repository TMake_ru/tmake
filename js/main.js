 
$('span').tooltip('hide');

$("#up-top").hide();
$(window).scroll(function (){ 
	if ($(this).scrollTop() > 100){
		$('#up-top').fadeIn();
	} else {
		$('#up-top').fadeOut();
	}
});

$("#up-top").click(function(){ 
	$('html, body').animate({scrollTop: "0px"}); 
});

$('.blog-portfolio-preview, .prtf-list-preview').hover(function(){
	$(this).find('.blog-portfolio-image-alt').fadeIn();
}, function(){
	$(this).find('.blog-portfolio-image-alt').fadeOut();
});
$('.prtf-list-preview').hover(function(){
	$(this).find('.prtf-list-image-alt').fadeIn();
}, function(){
	$(this).find('.prtf-list-image-alt').fadeOut();
});

$('.carousel').hover(function(){
	$(this).find('.gallery-button >a> span').fadeIn();
}, function(){
	$(this).find('.gallery-button >a> span').fadeOut();
});

$('.errorMessage').click(function(){
    $(this).css('display','none');
});