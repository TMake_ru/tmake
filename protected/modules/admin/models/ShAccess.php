<?php

class ShAccess
{
  private static $_model = array();

  /**
   * Singleton
   * @param string $className
   * @return object
   */
  public static function model($className=__CLASS__)
  {
      if(empty(self::$_model[$className]))
          self::$_model[$className] = new $className;
      return self::$_model[$className];
  }

  /**
   * Поиск активного пользователя по email
   * @param string $email
   * @return object
   */
  public function getUserForLogin($email){
      return Users::model()->find('email=:email AND type=:type',array(
            ':email'=>$email,
            ':type'=>Users::TYPE_ACTIVE
        ));
  }

  /**
   * Hash пароля
   * @param string $password
   * @param string $email
   * @return string
   */
  public function getHashPasswd($password, $email){
      return md5($password.md5($email));
  }

  /**
   * Доступ для авторов
   * @param int $uid
   * @return boolean
   */
  public function accessAuthor($uid = false){
    if(!$uid && !Yii::app()->user->isGuest)
      $uid = Yii::app()->user->id;

    if($uid && !empty($_GET['aliasShop'])){
      $model = Shop::model()->find('user_id=:uid AND alias=:alias',array(
          ':uid'=>$uid,
          ':alias'=>$_GET['aliasShop']));
      return ($model != NULL)? true: false;
    }
    return false;
  }
  
  public function trowAuthor($obj=false, $id=NULL){
    //if(!$this->accessAuthor())
    //  throw new CHttpException(404,'The requested page does not exist.');
    if(!$obj){
      $model = $obj->findByPk($id);
      if($model != NULL){
        if($model->shop_id != Shop::model()->getRow('id'))
          throw new CHttpException(404,'The requested page does not exist.');
      }
      else
        throw new CHttpException(404,'The requested page does not exist.');
    }
  }

   /**
   * Определение класса для меню
   * @param string $controller
   * @param string $action
   * @return string
   */
  public function classOfMenu($controller, $action=false, $get=false){
    if($action){
      if(Yii::app()->controller->id == $controller && Yii::app()->controller->action->id == $action){
        if($get){
          if(!empty($_GET[$get[0]]))
            if($_GET[$get[0]] == $get[1])
              return 'active';
          return '';
        }
        else
          return 'active';
      }
    }
    elseif(is_array($controller)){
      if(in_array(Yii::app()->controller->id, $controller))
        return 'active';  
    }
    elseif(Yii::app()->controller->id == $controller)
      return 'active';
    return '';
  }
    
  /**
   * Получаем alias магазина
   * @return string
   * @throws CHttpException
   */
  public function getAlias(){
    if(empty($_GET['aliasShop']))
      throw new CHttpException(404,'The requested page does not exist.');
    return $_GET['aliasShop'];
  }
  
  /**
   * Получаем язык
   * @return string
   */
  public function getLang(){
    return Yii::app()->request->getQuery('lang','ru');
  }
  
  /**
   * Получение статического перевода
   * @param string $field
   * @param mix $lang
   * @return string
   */
  public function t($field,$lang=false){
    if(!$lang)
      $lang = $this->getLang();
    return Yii::app()->params['shop-'.$lang][$field];
  }
}