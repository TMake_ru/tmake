<?php

class PortfolioController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('view','create','update','admin','delete','ajaxDeletePreview',
                    'deleteCode','updatePositionCode', 'updatePositionGallery','deleteGallery',
                    'deleteComment','updateComment'),
				'users'=>array('@'),
                'expression'=>'AdmAccess::model()->accessAdmin()'
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    
    /**
     * Изменение позиции примера кода
     */
    public function actionUpdatePositionCode()
    {
        if(!empty($_GET['action']) && !empty($_GET['id'])){
            $model = PrtfCode::model()->loadModel($_GET['id']);
            if($model->position > 1 && $_GET['action'] == 'up'){
                $up = PrtfCode::model()->find('prtf_id=:pid AND position=:pos',array(':pid'=>$model->prtf_id,':pos'=>$model->position-1));
                if($up != NULL){
                    $up->position = $model->position;
                    $up->save();
                    $model->position--;
                    $model->save();
                }
            }
            elseif($model->position < PrtfCode::model()->count('prtf_id=:pid',array(':pid'=>$model->prtf_id)) && $_GET['action'] == 'down'){
                $down = PrtfCode::model()->find('prtf_id=:pid AND position=:pos',array(':pid'=>$model->prtf_id,':pos'=>$model->position+1));
                if($down != NULL){
                    $down->position = $model->position;
                    $down->save();
                    $model->position++;
                    $model->save();
                }
            }

            $this->redirect(array('/admin/portfolio/view/id/'.intval($model->prtf_id).'#code'));
        }
        $this->redirect(array('/admin/portfolio/admin'));
    }
    
    /**
     * Изменение позиции изображений в галлереи
     */
    public function actionUpdatePositionGallery(){
        if(!empty($_GET['action']) && !empty($_GET['id'])){
            $model = PrtfGallery::model()->loadModel($_GET['id']);
            if($model->position > 1 && $_GET['action'] == 'up'){
                $up = PrtfGallery::model()->find('prtf_id=:pid AND position=:pos',array(':pid'=>$model->prtf_id,':pos'=>$model->position-1));
                if($up != NULL){
                    $up->position = $model->position;
                    $up->save();
                    $model->position--;
                    $model->save();
                }
            }
            elseif($model->position < PrtfGallery::model()->count('prtf_id=:pid',array(':pid'=>$model->prtf_id)) && $_GET['action'] == 'down'){
                $down = PrtfGallery::model()->find('prtf_id=:pid AND position=:pos',array(':pid'=>$model->prtf_id,':pos'=>$model->position+1));
                if($down != NULL){
                    $down->position = $model->position;
                    $down->save();
                    $model->position++;
                    $model->save();
                }
            }

            $this->redirect(array('/admin/portfolio/view/id/'.intval($model->prtf_id).'#gallery'));
        }
        $this->redirect(array('/admin/portfolio/admin'));
    }
    
    /**
     * Ajax удаление изображение
     */
    public function actionAjaxDeletePreview(){
       if(Yii::app()->request->isAjaxRequest){
           if(!empty($_POST['id'])){
               $model = $this->loadModel($_POST['id']);
               UploadImage::model()->deleteFile(Portfolio::DIR_FILE.$model->preview);
           }
           Yii::app()->end();
       }
       $this->redirect(array('/panele/portfolio/admin'));
    }
        
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model = $this->loadModel($id);
        
        // Code
        $modelCode = new PrtfCode;
        if(!empty($_POST['PrtfCode'])){
            $modelCode->attributes = $_POST['PrtfCode'];
            $modelCode->prtf_id = $model->id;
            $modelCode->position = PrtfCode::model()->countByAttributes(array('prtf_id'=>$model->id)) + 1;
            if($modelCode->save())
                $this->redirect('/admin/portfolio/view/id/'.intval($model->id).'#code');
        }
        $code = new PrtfCode('Search');
        $code->prtf_id = $model->id;
        
        // Gallery
        $modelGallery = new PrtfGallery;
        if(!empty($_POST['PrtfGallery'])){
            $modelGallery->attributes = $_POST['PrtfGallery'];
            $modelGallery->prtf_id = $model->id;
            $modelGallery->position = PrtfGallery::model()->countByAttributes(array('prtf_id'=>$model->id)) + 1;
            if($modelGallery->save())
                $this->redirect('/admin/portfolio/view/id/'.intval($model->id).'#gallery');
        }
        $criteria = new CDbCriteria;
        $criteria->addCondition('prtf_id=:pid');
        $criteria->params[':pid'] = $model->id;
        $criteria->order = 'position';
        $gallery = PrtfGallery::model()->findAll($criteria);
        
        // Comments
        $modelComment = new PrtfComments;
        if(!empty($_POST['PrtfComments'])){
            $modelComment->attributes = $_POST['PrtfComments'];
            $modelComment->prtf_id = $model->id;
            $modelComment->date_create = date('Y-m-d H:i:s');
            if($modelComment->save())
                $this->redirect('/admin/portfolio/view/id/'.intval($model->id).'#comments');
        }
        $comment = new PrtfComments('Search');
        $comment->prtf_id = $model->id;
        
		$this->render('view',array(
			'model'=>$model,
            'modelCode'=>$modelCode,
            'code'=>$code,
            'modelGallery'=>$modelGallery,
            'gallery'=>$gallery,
            'modelComment'=>$modelComment,
            'comment'=>$comment
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Portfolio;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Portfolio']))
		{
			$model->attributes=$_POST['Portfolio'];
            $model->date_create = date('Y-m-d H:i:s');
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Portfolio']))
		{
			$model->attributes=$_POST['Portfolio'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
    
    public function actionUpdateComment(){
        if(empty($_GET['id']))
            $this->redirect('/admin/portfolio/admin');
        $model = PrtfComments::model()->findByPk($_GET['id']);
        if($model == NULL)
            $this->redirect('/admin/portfolio/admin');
        
        if(!empty($_POST['PrtfComments'])){
            $model->attributes = $_POST['PrtfComments'];
            if($model->save())
                $this->redirect('/admin/portfolio/view/id/'.intval($model->prtf_id).'#comments');
        }
        
        $this->render('updateComment', array('model'=>$model));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
    
    /**
     * Удаление кода
     */
    public function actionDeleteCode(){
        if(!empty($_GET['id'])){
            $model = PrtfCode::model()->findByPk($_GET['id']);
            if($model != NULL){
                $prtfId = $model->prtf_id;
                $model->delete();
                $this->redirect('/admin/portfolio/view/id/'.intval($prtfId).'#code');
            }
        }
        $this->redirect('/admin/portfolio/admin');
    }
    
    /**
     * Удаление изображение из галереи
     */
    public function actionDeleteGallery(){
        if(!empty($_GET['id'])){
            $model = PrtfGallery::model()->findByPk($_GET['id']);
            if($model != NULL){
                $prtfId = $model->prtf_id;
                $model->delete();
                $this->redirect('/admin/portfolio/view/id/'.intval($prtfId).'#gallery');
            }
        }
        $this->redirect('/admin/portfolio/admin');
    }
    
    public function actionDeleteComment(){
        if(!empty($_GET['id'])){
            $model = PrtfComments::model()->findByPk($_GET['id']);
            if($model != NULL){
                $prtfId = $model->prtf_id;
                $model->delete();
                $this->redirect('/admin/portfolio/view/id/'.intval($prtfId).'#comments');
            }
        }
        $this->redirect('/admin/portfolio/admin');
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Portfolio('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Portfolio']))
			$model->attributes=$_GET['Portfolio'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Portfolio the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Portfolio::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Portfolio $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='portfolio-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
