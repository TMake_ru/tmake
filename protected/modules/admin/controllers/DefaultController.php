<?php

class DefaultController extends Controller
{
        public $layout = '//layouts/column2';
        
        /**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			////'postOnly + delete', // we only allow deletion via POST request
		);
	}
        
        /**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow',
                'actions'=>array('login','error','logout'),
                'users'=>array('*'),
            ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','uploadImperavi'),
				'users'=>array('@'),
                'expression'=>'AdmAccess::model()->accessAdmin()'
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    
    /**
     * Сохранение изображение из редактора
     */
    public function actionUploadImperavi(){
     if(!empty($_FILES) && !empty($_GET['modelName']) && !empty($_GET['field'])){
       $model = new UploadImage;
       $model->file = CUploadedFile::getInstanceByName('file');
       if($model->file != NULL)
       {
         $name = $model->getNewName().'.'.$model->file->getExtensionName();
         if($model->file->saveAs(Yii::getPathOfAlias('webroot').Imperavi::DIR_IMPERAVI.$name))
         {
           $obj = new Imperavi;
           $obj->image = $name;
           $obj->modelName = $_GET['modelName'];
           $obj->field = $_GET['field'];

           if(!empty($_GET['modelId'])){
             $obj->status = Imperavi::STAT_TRUE;
             $obj->modelId = $_GET['modelId'];
           }
           else
             $obj->status = Imperavi::STAT_FALSE;

           $obj->save();
         }

         $array = array(
           'filelink' => Imperavi::DIR_IMPERAVI.$name
         );
         echo stripslashes(json_encode($array));
         Yii::app()->end();
       }
     }
     $array = array(
         'filelink' => ''
     );
     echo stripslashes(json_encode($array));
     Yii::app()->end();
    }
    
    /**
     * Главная страница админ панели
     */
	public function actionIndex()
	{
		$this->render('index');
	}
        
    /**
     * Вывод ошибки
     */
    public function actionError()
	{
        if($error=Yii::app()->errorHandler->error)
            $this->renderPartial('error', $error);
	}
        
    /**
     * Авторизация пользователя
     */
    public function actionLogin()
    {
        $model=new LoginFormAdmin;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
                echo CActiveForm::validate($model);
                Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginFormAdmin']))
        {
                $model->attributes=$_POST['LoginFormAdmin'];
                // validate user input and redirect to the previous page if valid
                if($model->validate() && $model->login())
                        $this->redirect(array('/admin'));
        }
        $this->renderPartial('login',array('model'=>$model));
    }
        
    /**
	 * Сброс авторизации
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(array('/admin'));
	}
}