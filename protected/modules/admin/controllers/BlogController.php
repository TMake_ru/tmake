<?php

class BlogController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('view','create','update','admin','delete','ajaxDeletePreview', 'deleteCategory',
                    'deleteComment','updateComment'),
				'users'=>array('@'),
                'expression'=>'AdmAccess::model()->accessAdmin()'
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    
    /**
     * Ajax удаление изображение
     */
    public function actionAjaxDeletePreview(){
       if(Yii::app()->request->isAjaxRequest){
           if(!empty($_POST['id'])){
               $model = $this->loadModel($_POST['id']);
               UploadImage::model()->deleteFile(Blog::DIR_FILE.$model->preview);
           }
           Yii::app()->end();
       }
       $this->redirect(array('/panele/blog/admin'));
    }
        
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model = $this->loadModel($id);
        
        // Категории
        $modelCategory = new BlogCategory;
        $cat = new Category;
        if(!empty($_POST['BlogCategory'])){
            $modelCategory->attributes = $_POST['BlogCategory'];
            $modelCategory->blog_id = $model->id;
            if($modelCategory->category_id == '-1' && $_POST['Category']){
                $cat->attributes = $_POST['Category'];
                if($cat->save())
                    $modelCategory->category_id = $cat->id;
            }
            if($modelCategory->category_id != '-1' && BlogCategory::model()->countByAttributes(array('blog_id'=>$modelCategory->blog_id,'category_id'=>$modelCategory->category_id)) == 0)
                if($modelCategory->save())
                    $this->redirect('/admin/blog/view/id/'.intval($model->id).'#category');
        }
        
        $category = new BlogCategory('Search');
        $category->blog_id = $model->id;
        
        if(empty($modelCategory->category_id))
            $modelCategory->category_id = '-1';
        
        // Комментарии
        $modelComment = new BlogComments;
        if(!empty($_POST['BlogComments'])){
            $modelComment->attributes = $_POST['BlogComments'];
            $modelComment->blog_id = $model->id;
            $modelComment->date_create = date('Y-m-d H:i:s');
            $modelComment->ip = $_SERVER['REMOTE_ADDR'];
            if($modelComment->save()){
                $model->count_comment = BlogComments::model()->countByAttributes(array('blog_id'=>$model->id));
                if($model->save())
                    $this->redirect('/admin/blog/view/id/'.intval($model->id).'#comments');
            }
        }
        
        $comment = new BlogComments('Search');
        $comment->blog_id = $model->id;
        
		$this->render('view',array(
			'model'=>$model,
            'modelCategory'=>$modelCategory,
            'cat'=>$cat,
            'category'=>$category,
            'modelComment'=>$modelComment,
            'comment'=>$comment
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Blog;
        $model->scenario = 'adminCreate';
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Blog']))
		{
			$model->attributes=$_POST['Blog'];
            $model->date_create = date('Y-m-d H:i:s');
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $model->scenario = 'adminUpdate';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Blog']))
		{
			$model->attributes=$_POST['Blog'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
    
    public function actionUpdateComment(){
        if(empty($_GET['id']))
            $this->redirect('/admin/blog/admin');
        $model =  BlogComments::model()->findByPk($_GET['id']);
        if($model == NULL)
            $this->redirect('/admin/blog/admin');
        
        if(!empty($_POST['BlogComments'])){
            $model->attributes = $_POST['BlogComments'];
            if($model->save()){
                $blog = Blog::model()->findByPk($model->blog_id);
                if($blog != NULL){
                    $blog->count_comment = BlogComments::model()->countByAttributes(array('blog_id'=>$model->blog_id));
                    $blog->save();
                }
                $this->redirect('/admin/blog/view/id/'.intval($model->blog_id).'#comments');
            }
        }
        
        $this->render('updateComment', array('model'=>$model));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
    
    /**
     * Удаление связи категория-блог
     */
    public function actionDeleteCategory(){
        if(!empty($_GET['id'])){
            $model = BlogCategory::model()->findByPk($_GET['id']);
            if($model != NULL){
                $blogId = $model->blog_id;
                $model->delete();
                $this->redirect('/admin/blog/view/id/'.intval($blogId).'#category');
            }
        }
        $this->redirect('/admin/blog/admin');
    }
    
    /**
     * Удаление комментария к блогу
     */
    public function actionDeleteComment(){
        if(!empty($_GET['id'])){
            $model = BlogComments::model()->findByPk($_GET['id']);
            if($model != NULL){
                $blogId = $model->blog_id;
                $model->delete();
                $this->redirect('/admin/blog/view/id/'.intval($blogId).'#category');
            }
        }
        $this->redirect('/admin/blog/admin');
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Blog('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Blog']))
			$model->attributes=$_GET['Blog'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Blog the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Blog::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Blog $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='blog-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
