<?php $this->widget('AdmGridView', array(
    'id'=>'commeent-blog-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        'date_create',
        'username',
        array(
            'type'=>'raw',
            'name'=>'text_description',
        ),
        'ip',
        'rating',
        array(
            'class'=>'CButtonColumn',
            'template'=>'{update}{delete}',
                'buttons'=>array(
                    'update'=>array(
                        'label'=>'<i class="icon-pencil"></i>',
                        'url'=>'Yii::app()->createUrl(\'/admin/blog/updateComment/\',array(\'id\'=>intval($id)))',
                        'color'=>'blue',
                    ),
                    'delete'=>array(
                        'label'=>'<i class="icon-trash"></i>',
                        'url'=>'Yii::app()->createUrl(\'/admin/blog/deleteComment/\',array(\'id\'=>intval($id)))',
                        'color'=>'red',
                    ),
                ),
        ),
    ),
)); ?>