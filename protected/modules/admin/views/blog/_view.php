<?php
/* @var $this BlogController */
/* @var $data Blog */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('header')); ?>:</b>
	<?php echo CHtml::encode($data->header); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keywords')); ?>:</b>
	<?php echo CHtml::encode($data->keywords); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alias')); ?>:</b>
	<?php echo CHtml::encode($data->alias); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('short_description')); ?>:</b>
	<?php echo CHtml::encode($data->short_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text_description')); ?>:</b>
	<?php echo CHtml::encode($data->text_description); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('date_create')); ?>:</b>
	<?php echo CHtml::encode($data->date_create); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('preview')); ?>:</b>
	<?php echo CHtml::encode($data->preview); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('count_view')); ?>:</b>
	<?php echo CHtml::encode($data->count_view); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('count_like')); ?>:</b>
	<?php echo CHtml::encode($data->count_like); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('count_dislike')); ?>:</b>
	<?php echo CHtml::encode($data->count_dislike); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('count_comment')); ?>:</b>
	<?php echo CHtml::encode($data->count_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	*/ ?>

</div>