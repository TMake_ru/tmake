<?php $this->widget('AdmGridView', array(
    'id'=>'category-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        array(
            'name'=>'category_id',
            'value'=>'Category::model()->getField($data->category_id)'
        ),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{delete}',
                'buttons'=>array(
                    'delete'=>array(
                        'label'=>'<i class="icon-trash"></i>',
                        'url'=>'Yii::app()->createUrl(\'/admin/blog/deleteCategory/\',array(\'id\'=>intval($id)))',
                        'color'=>'red',
                    ),
                ),
        ),
    ),
)); ?>
