<?php 
Yii::app()->clientScript->registerScript('script-category-blog',"
    $('select[name=\'BlogCategory[category_id]\']').change(function(){
        if($(this).val() == '-1')
            $('.new-category').css('display','block');
        else
            $('.new-category').css('display','none');
    });
");
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'blog-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('class'=>'form-horizontal'),
)); ?>

	<p class="alert"><span class="required">*</span> Поля обязательные для заполнения.</p>

	<?php if($model->hasErrors()):?>
        <div class="alert alert-error">
                <button class="close" data-dismiss="alert"></button>
                <?php echo $form->errorSummary($model); ?>
        </div>
    <?php endif;?>
    
    <div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'category_id'); ?>
	    </label>
	    <div class="controls">
    		<?php echo $form->dropDownList($model,'category_id',Category::model()->getFieldAll(true,array('-1'=>' - Добавить - '))); ?>
            <div class="new-category" <?php if($model->category_id != '-1') echo 'style="display:none;"';?>>
                <?php echo CHtml::activeTextField($cat,'title_name',array('placeholder'=>'Название категории'));?>
                <?php echo CHtml::error($cat, 'title_name');?>
                <?php echo CHtml::activeTextField($cat,'alias',array('placeholder'=>'Alias категории'));?>
                <?php echo CHtml::error($cat, 'alias');?>
            </div>
            <?php echo $form->error($model,'category_id',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

    <div class="form-actions">
	    <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить',array('class'=>'btn blue')); ?>
	</div>

<?php $this->endWidget(); ?>