<?php
/* @var $this BlogController */
/* @var $model Blog */

$this->pageTitle = 'Блог';
$this->breadcrumbs=array(
	'Блог'=>array('admin'),
	CHtml::encode($model->header),
);

$this->menu=array(
	array('label'=>'Добавить', 'url'=>array('create')),
	array('label'=>'Изменить', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить', 'url'=>array('delete', 'id'=>$model->id)),
	array('label'=>'Менеджер', 'url'=>array('admin')),
);
?>

<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-star"></i>Просмотреть</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">

            <?php $this->widget('zii.widgets.CDetailView', array(
                'data'=>$model,
                'attributes'=>array(
                    'id',
                    'header',
                    'keywords',
                    'description',
                    'alias',
                    'short_description',
                    array(
                        'type'=>'raw',
                        'name'=>'text_description'
                    ),
                    'date_create',
                    array(
                        'type'=>'raw',
                        'name'=>'preview',
                        'value'=>  UploadImage::model()->getImage(Blog::DIR_FILE.$model->preview)
                    ),
                    'count_view',
                    'count_like',
                    'count_dislike',
                    'count_comment',
                    array(
                        'name'=>'status',
                        'value'=>Blog::model()->getStatus($model->status)
                    ),
                ),
            )); ?>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<a name="category"></a>
<div class="row-fluid">
    <div class="span6">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption"><i class="icon-star"></i>Добавить категорию</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">
                <?php $this->renderPartial('_formCategory',array('model'=>$modelCategory, 'cat'=>$cat));?>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
    
    <div class="span6">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption"><i class="icon-star"></i>Категории</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">
                <?php $this->renderPartial('_category',array('model'=>$category));?>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<a name="comments"></a>
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption"><i class="icon-star"></i>Добавить комментарий</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">
                <?php $this->renderPartial('_formComment',array('model'=>$modelComment));?>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption"><i class="icon-star"></i>Комментарии</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">
                <?php $this->renderPartial('_comment',array('model'=>$comment));?>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>