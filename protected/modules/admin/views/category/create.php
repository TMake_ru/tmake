<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->pageTitle = 'Категории';
$this->breadcrumbs=array(
	'Добавить',
);

$this->menu=array(
	array('label'=>'Менеджер', 'url'=>array('admin')),
);
?>

<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-plus"></i>Добавить</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>