<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'htmlOptions'=>array('class'=>'form-horizontal'),
)); ?>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'id'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'id',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'title_name'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'title_name',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'alias'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'alias',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'count_blog'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->numberField($model,'count_blog',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <?php echo CHtml::submitButton('Поиск',array('class'=>'btn blue')); ?>
	</div>

<?php $this->endWidget(); ?>