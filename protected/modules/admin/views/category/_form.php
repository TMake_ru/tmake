<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'category-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('class'=>'form-horizontal'),
)); ?>

	<p class="alert"><span class="required">*</span> Поля обязательные для заполнения.</p>

	<?php if($model->hasErrors()):?>
        <div class="alert alert-error">
                <button class="close" data-dismiss="alert"></button>
                <?php echo $form->errorSummary($model); ?>
        </div>
        <?php endif;?>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'title_name'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'title_name',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'title_name',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'alias'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'alias',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'alias',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'count_blog'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->numberField($model,'count_blog'); ?>
		<?php echo $form->error($model,'count_blog',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

	<div class="form-actions">
	    <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить',array('class'=>'btn blue')); ?>
	</div>

<?php $this->endWidget(); ?>