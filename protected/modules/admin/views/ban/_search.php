<?php
/* @var $this BanController */
/* @var $model Ban */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript('script-data',"
    $('select[name=\'Ban[type]\']').change(function(){
        var type = $(this).val();
        if(type == '')
            $('#block-data').css('display','none');
        else
            $('#block-data').css('display','block');
        
        $.ajax({
            type: 'POST',
            data: 'first=true&type='+type,
            url: '".Yii::app()->createUrl('/admin/Ban/ajaxDataName')."',
            success: function(q){
                $('select[name=\'Ban[data_id]\']').html(q);
            }
        });
    });
");
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'htmlOptions'=>array('class'=>'form-horizontal'),
)); ?>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'id'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'id',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'type'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->dropDownList($model,'type',Ban::model()->getTypeAll(true), array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group"  id="block-data" <?php if(empty($model->type)) echo 'style="display:none;"';?>>
	    <label class="control-label">
		<?php echo $form->label($model,'data_id'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->dropDownList($model,'data_id',(!empty($model->type))? Ban::model()->getNameData(0, $model->type, true,true):array(),array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'ip'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'ip',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'date_create'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->dateField($model,'date_create',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <?php echo CHtml::submitButton('Поиск',array('class'=>'btn blue')); ?>
	</div>

<?php $this->endWidget(); ?>