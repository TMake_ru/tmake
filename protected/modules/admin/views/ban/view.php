<?php
/* @var $this BanController */
/* @var $model Ban */

$this->pageTitle = 'Запрет';
$this->breadcrumbs=array(
	'Запрет'=>array('admin'),
	CHtml::encode($model->ip),
);

$this->menu=array(
	array('label'=>'Добавить', 'url'=>array('create')),
	array('label'=>'Изменить', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить', 'url'=>array('delete', 'id'=>$model->id)),
	array('label'=>'Менеджер', 'url'=>array('admin')),
);
?>

<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-plus"></i>Просмотреть</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        array(
            'name'=>'type',
            'value'=>Ban::model()->getType($model->type),
        ),
        array(
            'name'=>'data_id',
            'value'=>Ban::model()->getNameData($model->data_id, $model->type)
        ),
		'ip',
		'date_create',
	),
)); ?>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
