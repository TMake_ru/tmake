<?php
/* @var $this BanController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bans',
);

$this->menu=array(
	array('label'=>'Create Ban', 'url'=>array('create')),
	array('label'=>'Manage Ban', 'url'=>array('admin')),
);
?>

<h1>Bans</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
