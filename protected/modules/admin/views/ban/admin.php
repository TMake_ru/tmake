<?php
/* @var $this BanController */
/* @var $model Ban */

$this->pageTitle = 'Запрет';
$this->breadcrumbs=array(
	'Запрет',
);

$this->menu=array(
	array('label'=>'Добавить', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#ban-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-cloud"></i>Запрет</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">
                
                <div class="portlet box grey span6">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-search"></i>
                        <?php echo CHtml::link('Поиск','#',array('class'=>'search-button','style'=>'color:#eee;')); ?>
                        </div>
                    </div>
                    <div class="portlet-body search-form" style="display:none">
                        <?php $this->renderPartial('_search',array(
                            'model'=>$model,
                        )); ?>  
                    </div>
                </div>
                <!-- search-form -->

<?php $this->widget('AdmGridView', array(
	'id'=>'ban-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
        array(
            'name'=>'type',
            'value'=>'Ban::model()->getType($data->type)'
        ),
        array(
            'name'=>'data_id',
            'value'=>'Ban::model()->getNameData($data->data_id, $data->type)'
        ),
		'ip',
		'date_create',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
