<?php
/* @var $this BanController */
/* @var $model Ban */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript('script-data',"
    $('select[name=\'Ban[type]\']').change(function(){
        var type = $(this).val();
        $.ajax({
            type: 'POST',
            data: 'type='+type,
            url: '".Yii::app()->createUrl('/admin/ban/ajaxDataName')."',
            success: function(q){
                $('select[name=\'Ban[data_id]\']').html(q);
            }
        });
    });
");
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ban-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('class'=>'form-horizontal'),
)); ?>

	<p class="alert"><span class="required">*</span> Поля обязательные для заполнения.</p>

	<?php if($model->hasErrors()):?>
        <div class="alert alert-error">
                <button class="close" data-dismiss="alert"></button>
                <?php echo $form->errorSummary($model); ?>
        </div>
    <?php endif;?>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'type'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->dropDownList($model,'type', Ban::model()->getTypeAll()); ?>
		<?php echo $form->error($model,'type',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'data_id'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->dropDownList($model,'data_id', Ban::model()->getNameData(0, $model->type, true)); ?>
		<?php echo $form->error($model,'data_id',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'ip'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'ip',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'ip',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

	<div class="form-actions">
	    <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить',array('class'=>'btn blue')); ?>
	</div>

<?php $this->endWidget(); ?>