<?php
/* @var $this IpControlController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ip Controls',
);

$this->menu=array(
	array('label'=>'Create IpControl', 'url'=>array('create')),
	array('label'=>'Manage IpControl', 'url'=>array('admin')),
);
?>

<h1>Ip Controls</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
