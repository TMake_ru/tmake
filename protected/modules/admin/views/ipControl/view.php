<?php
/* @var $this IpControlController */
/* @var $model IpControl */

$this->pageTitle = 'IP Контроль';
$this->breadcrumbs=array(
	'IP Контроль'=>array('admin'),
	$model->ip,
);

$this->menu=array(
	array('label'=>'Добавить', 'url'=>array('create')),
	array('label'=>'Изменить', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить', 'url'=>array('delete', 'id'=>$model->id)),
	array('label'=>'Менеджер', 'url'=>array('admin')),
);
?>

<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-plus"></i>Просмотреть</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">

                <?php $this->widget('zii.widgets.CDetailView', array(
                    'data'=>$model,
                    'attributes'=>array(
                        'id',
                        array(
                            'name'=>'type',
                            'value'=>IpControl::model()->getType($model->type)
                        ),
                        array(
                            'name'=>'data_id',
                            'value'=>  IpControl::model()->getNameData($model->data_id, $model->type),
                        ),
                        'ip',
                        'date_create',
                    ),
                )); ?>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
