<?php
/* @var $this PortfolioController */
/* @var $model Portfolio */

$this->pageTitle = 'Портфолио';
$this->breadcrumbs=array(
	'Портфолио',
);

$this->menu=array(
	array('label'=>'Добавить', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#portfolio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-cloud"></i>Портфолио</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">
                
                <div class="portlet box grey span6">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-search"></i>
                        <?php echo CHtml::link('Поиск','#',array('class'=>'search-button','style'=>'color:#eee;')); ?>
                        </div>
                    </div>
                    <div class="portlet-body search-form" style="display:none">
                        <?php $this->renderPartial('_search',array(
                            'model'=>$model,
                        )); ?>  
                    </div>
                </div>
                <!-- search-form -->

                <?php $this->widget('AdmGridView', array(
                    'id'=>'portfolio-grid',
                    'dataProvider'=>$model->search(),
                    'filter'=>$model,
                    'columns'=>array(
                        'id',
                        'header',
                        'alias',
                        array(
                            'type'=>'raw',
                            'name'=>'url',
                            'value'=>'CHtml::link($data->url, CHtml::encode($data->url),array(\'target\'=>\'_blank\'))'
                        ),
                        'date_create',
                        'count_view',
                        'count_comment',
                        array(
                            'class'=>'CButtonColumn',
                        ),
                    ),
                )); ?>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
