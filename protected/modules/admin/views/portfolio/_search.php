<?php
/* @var $this PortfolioController */
/* @var $model Portfolio */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'htmlOptions'=>array('class'=>'form-horizontal'),
)); ?>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'id'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'id',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'header'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'header',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'keywords'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'keywords',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'description'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'description',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'alias'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'alias',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'url'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'url',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'count_view'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->numberField($model,'count_view',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'count_like'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->numberField($model,'count_like',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'count_dislike'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->numberField($model,'count_dislike',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'count_comment'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->numberField($model,'count_comment',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->label($model,'date_create'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->dateField($model,'date_create',array('class'=>'span12')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <?php echo CHtml::submitButton('Поиск',array('class'=>'btn blue')); ?>
	</div>

<?php $this->endWidget(); ?>