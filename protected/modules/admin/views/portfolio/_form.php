<?php
/* @var $this PortfolioController */
/* @var $model Portfolio */
/* @var $form CActiveForm */
Yii::import('ext.imperavi.ImperaviRedactorWidget');
$this->widget('ImperaviRedactorWidget', array(
    // the textarea selector
    'selector' => '.redactor',
    // some options, see http://imperavi.com/redactor/docs/
    'options' => array(
        'imageUpload'=>($model->isNewRecord)? 
          Yii::app()->createUrl('/admin/default/uploadImperavi?modelName=Portfolio&field=text_description'): 
          Yii::app()->createUrl('/admin/default/uploadImperavi?modelName=Portfolio&field=text_description&modelId='.intval($model->id)),
        'lang' => 'ru',
        'minHeight'=>250
    ),
));

if(!$model->isNewRecord)
    Yii::app()->clientScript->registerScript('update-news-form',"
        $('#delete-file').click(function(){
            $.ajax({
                type: 'POST',
                url: '".Yii::app()->createUrl('/admin/portfolio/ajaxDeletePreview')."',
                data: 'id=".intval($model->id)."'
            });
            $(this).parent().empty();
        });
    ");
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'portfolio-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('class'=>'form-horizontal','enctype'=>'multipart/form-data'),
)); ?>

	<p class="alert"><span class="required">*</span> Поля обязательные для заполнения.</p>

	<?php if($model->hasErrors()):?>
        <div class="alert alert-error">
                <button class="close" data-dismiss="alert"></button>
                <?php echo $form->errorSummary($model); ?>
        </div>
    <?php endif;?>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'header'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'header',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'header',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'keywords'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'keywords',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'keywords',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'description'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>400)); ?>
		<?php echo $form->error($model,'description',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'alias'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'alias',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'alias',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'url'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'url',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'count_view'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->numberField($model,'count_view'); ?>
		<?php echo $form->error($model,'count_view',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'count_like'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->numberField($model,'count_like'); ?>
		<?php echo $form->error($model,'count_like',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'count_dislike'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->numberField($model,'count_dislike'); ?>
		<?php echo $form->error($model,'count_dislike',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'count_comment'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->numberField($model,'count_comment'); ?>
		<?php echo $form->error($model,'count_comment',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'text_description'); ?>
	    </label>
	    <div class="controls">
		<?php echo $form->textArea($model,'text_description',array('class'=>'redactor')); ?>
		<?php echo $form->error($model,'text_description',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">
		<?php echo $form->labelEx($model,'upload_preview'); ?>
	    </label>
	    <div class="controls">
            <?php if(UploadImage::model()->isFile(Portfolio::DIR_FILE.$model->preview)):?>
                <div>
                    <?php echo EHtml::image(Portfolio::DIR_FILE.$model->preview);?>
                    <br><?php echo CHtml::link('Удалить','javascript:void(0);',array('id'=>'delete-file'));?>
                </div>
            <?php endif;?>
            <?php echo $form->fileField($model,'upload_preview',array('size'=>60,'maxlength'=>150)); ?>
            <?php echo $form->error($model,'upload_preview',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>

	<div class="form-actions">
	    <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить',array('class'=>'btn blue')); ?>
	</div>

<?php $this->endWidget(); ?>