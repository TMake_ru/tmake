<?php
/* @var $this PortfolioController */
/* @var $model PrtfComments */

$this->pageTitle = 'Комментарий к портфолио';
$this->breadcrumbs=array(
	'Портфолио'=>array('admin'),
	CHtml::encode(Portfolio::model()->getField($model->prtf_id))=>array('view','id'=>$model->prtf_id),
	'Комментарий '.CHtml::encode($model->date_create),
);

$this->menu=array(
	array('label'=>'Добавить', 'url'=>array('create')),
	array('label'=>'Изменить', 'url'=>array('update', 'id'=>$model->prtf_id)),
	array('label'=>'Удалить', 'url'=>array('delete', 'id'=>$model->prtf_id)),
	array('label'=>'Менеджер', 'url'=>array('admin')),
);
?>

<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-plus"></i>Изменить комментарий</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo $this->renderPartial('_formComment', array('model'=>$model)); ?>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>