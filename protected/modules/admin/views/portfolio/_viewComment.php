<?php $this->widget('AdmGridView', array(
    'id'=>'prtf-comments-grid',
    'dataProvider'=>$model->search(),
    'columns'=>array(
        'id',
        'date_create',
        array(
            'type'=>'raw',
            'name'=>'text_description',
        ),
        'rating',
        array(
            'class'=>'CButtonColumn',
            'template'=>'{update}{delete}',
                'buttons'=>array(
                    'update'=>array(
                        'label'=>'<i class="icon-pencil"></i>',
                        'url'=>'Yii::app()->createUrl(\'/admin/portfolio/updateComment/\',array(\'id\'=>intval($id)))',
                        'color'=>'blue',
                    ),
                    'delete'=>array(
                        'label'=>'<i class="icon-trash"></i>',
                        'url'=>'Yii::app()->createUrl(\'/admin/portfolio/deleteComment/\',array(\'id\'=>intval($id)))',
                        'color'=>'red',
                    ),
                ),
        ),
    ),
)); ?>