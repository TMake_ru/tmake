<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'prtf-gallery-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('class'=>'form-horizontal','enctype'=>'multipart/form-data'),
)); ?>

	<p class="alert"><span class="required">*</span> Поля обязательные для заполнения.</p>

	<?php if($model->hasErrors()):?>
        <div class="alert alert-error">
                <button class="close" data-dismiss="alert"></button>
                <?php echo $form->errorSummary($model); ?>
        </div>
    <?php endif;?>

    <div class="control-group">
        <label class="control-label">
            <?php echo $form->labelEx($model,'upload_image'); ?>
	    </label>
	    <div class="controls">
            <?php echo $form->fileField($model,'upload_image'); ?>
            <?php echo $form->error($model,'upload_image',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>
    
    <div class="control-group">
        <label class="control-label">
            <?php echo $form->labelEx($model,'alt'); ?>
	    </label>
	    <div class="controls">
            <?php echo $form->textField($model,'alt'); ?>
            <?php echo $form->error($model,'alt',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>
    
    <div class="form-actions">
	    <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить',array('class'=>'btn blue')); ?>
	</div>

<?php $this->endWidget(); ?>