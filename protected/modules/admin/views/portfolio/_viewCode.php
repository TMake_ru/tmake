<style>
    .block-code-description{
        width: 600px;
        height: 250px;
        overflow-y: scroll;
    }
</style>

<?php $this->widget('AdmGridView', array(
    'id'=>'prtf-code-grid',
    'dataProvider'=>$model->search(),
    'switchFormRedirect'=>true,
    'routeSort'=>'PrtfCode',
    'positionButton'=>true,
    'positionData'=>array(
        'routeUp'=>'/admin/portfolio/updatePositionCode?action=up&id=$data->id',
        'routeDown'=>'/admin/portfolio/updatePositionCode?action=down&id=$data->id',
    ),
    'columns'=>array(
        'id',
        'header',
        array(
            'type'=>'raw',
            'name'=>'code_description',
            'value'=>'\'<div class="block-code-description">\'.$data->code_description.\'</div>\''
        ),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{delete}',
                'buttons'=>array(
                    'delete'=>array(
                        'label'=>'<i class="icon-trash"></i>',
                        'url'=>'Yii::app()->createUrl(\'/admin/portfolio/deleteCode/\',array(\'id\'=>intval($id)))',
                        'color'=>'red',
                    ),
                ),
        ),
    ),
)); ?>