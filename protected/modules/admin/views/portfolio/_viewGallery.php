<?php 
Yii::app()->clientScript->registerCssFile('/assets/plugins/fancybox/source/jquery.fancybox.css');
Yii::app()->clientScript->registerScriptFile('/assets/plugins/fancybox/source/jquery.fancybox.pack.js',  CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScript('banner-script',"
    var Gallery = function () {

        return {
            //main function to initiate the module
            init: function () {

                App.initFancybox();

            }

        };

    }();
    		   Gallery.init();
		   $('.fancybox-video').fancybox({type: 'iframe'});

");
?>

<?php if(count($model)>0): $i=1;?>
<?php foreach($model as $unit):?>
    <?php if($i == 1) echo '<div class="row-fluid">';?>

    <?php if(UploadImage::model()->isFile(PrtfGallery::DIR_FILE.$unit->image)):?>
    <div class="span3">
        <div class="item">
            <a class="fancybox-button" data-rel="fancybox-button" title="<?php echo CHtml::encode($unit->alt);?>" href="<?php echo PrtfGallery::DIR_FILE.$unit->image;?>">
                <div class="zoom">
                    <img src="<?php echo PrtfGallery::DIR_FILE.$unit->image;?>" alt="<?php echo CHtml::encode($unit->alt);?>">                    
                    <div class="zoom-icon"></div>
                </div>
            </a>
            <div class="details">
                <a href="<?php echo Yii::app()->createUrl('/admin/portfolio/updatePositionGallery',array('action'=>'up','id'=>$unit->id));?>" class="icon"><i class="m-icon-swapup m-icon-white"></i></a>
                <a href="<?php echo Yii::app()->createUrl('/admin/portfolio/updatePositionGallery',array('action'=>'down','id'=>$unit->id));?>" class="icon"><i class="m-icon-swapdown m-icon-white"></i></a>


                <a href="<?php echo Yii::app()->createUrl('/admin/portfolio/deleteGallery',array('id'=>$unit->id));?>" class="icon"><i class="icon-remove"></i></a>    
            </div>
        </div>
    </div>
    <?php endif;?>

<?php  if($i == 4){ echo '</div>'; $i=1; }else{ $i++;}?>
<?php endforeach;?>
<?php if($i != 1) echo '</div>';?>
<?php endif;?>