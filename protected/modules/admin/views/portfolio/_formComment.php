<?php 
Yii::import('ext.imperavi.ImperaviRedactorWidget');
$this->widget('ImperaviRedactorWidget', array(
    // the textarea selector
    'selector' => '.redactor',
    // some options, see http://imperavi.com/redactor/docs/
    'options' => array(
        'lang' => 'ru',
        'minHeight'=>250
    ),
));
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'prtf-comment-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('class'=>'form-horizontal'),
)); ?>

	<p class="alert"><span class="required">*</span> Поля обязательные для заполнения.</p>

	<?php if($model->hasErrors()):?>
        <div class="alert alert-error">
                <button class="close" data-dismiss="alert"></button>
                <?php echo $form->errorSummary($model); ?>
        </div>
    <?php endif;?>

    <div class="control-group">
        <label class="control-label">
            <?php echo $form->labelEx($model,'text_description'); ?>
	    </label>
	    <div class="controls">
            <?php echo $form->textArea($model,'text_description',array('class'=>'redactor')); ?>
            <?php echo $form->error($model,'text_description',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>
    
    <div class="control-group">
        <label class="control-label">
            <?php echo $form->labelEx($model,'rating'); ?>
	    </label>
	    <div class="controls">
            <?php echo $form->numberField($model,'rating'); ?>
            <?php echo $form->error($model,'rating',array('class'=>'alert alert-error')); ?>
	    </div>
	</div>
    
    <div class="form-actions">
	    <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить',array('class'=>'btn blue')); ?>
	</div>

<?php $this->endWidget(); ?>