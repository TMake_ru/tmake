<?php

class SiteController extends Controller
{
    public $layout = '//layouts/column2';
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
        $criteria = new CDbCriteria;
        $criteria->order = 'date_create DESC';
        $criteria->limit = 6;
		$prtf = Portfolio::model()->findAll($criteria);
        
        $criteria = new CDbCriteria;
        $criteria->addCondition('status=:stat');
        $criteria->params[':stat'] = Blog::STAT_PUB;
        $criteria->order = 'date_create DESC';
        $criteria->limit = 4;
        $blog = Blog::model()->findAll($criteria);
        
        $form = new ContactForm;
        if(!empty($_POST['ContactForm'])){
            $form->attributes = $_POST['ContactForm'];
            if($form->validate()){
                $message = $this->renderPartial('feedback', array('model'=>$form), true);
                if(Qwe::model()->mail('TMake.ru: Обратная связь', $form->email, $message)){
                    Yii::app()->user->setFlash('feedback','Письмо отправлено. В течении суток Вам ответят.');
                    $this->refresh();
                }
            }
        }
        
		$this->render('index',array(
            'prtf'=>$prtf,
            'blog'=>$blog,
            'form' => $form,
        ));
	}
    
    /**
     * Поиск
     */
    public function actionSearch()
    {   
        // Портфолио
        $criteria = new CDbCriteria;
        if(!empty($_POST['s'])){
            $criteria->addCondition('header LIKE :search');
            $criteria->params[':search'] = '%'.$_POST['s'].'%';
        }
        $criteria->order = 'date_create DESC';
        $portfolio = Portfolio::model()->findAll($criteria);
        
        // Блог
        $criteria = new CDbCriteria;
        if(!empty($_GET['category'])){
            $criteria->addInCondition('category.alias', $_GET['category']);
        }
        if(!empty($_POST['s'])){
            $criteria->addCondition('header LIKE :search');
            $criteria->params[':search'] = '%'.$_POST['s'].'%';
        }
        $criteria->order = 't.date_create DESC';
        $blog = Blog::model()->with(array('blogCategories'=>array('with'=>'category')))->findAll($criteria);
        
        // Категории
        $criteria = new CDbCriteria;
        if(!empty($_GET['category'])){
            $criteria->addInCondition('alias', $_GET['category']);
        }
        $category = Category::model()->findAll($criteria);
        
		$this->render('search',array(
            'prtf'=>$portfolio,
            'blog'=>$blog,
            'category'=>$category
        ));
    }
    
    /**
     * Расценки
     */
    public function actionPrice()
    {
        $model = new ContactForm;
        
        $this->render('price',array(
            'model'=>$model
        ));
    }
    
    public function actionFeedback(){
        if(Yii::app()->request->isAjaxRequest){
            if(!empty($_POST['ContactForm'])){
                $model = new ContactForm;
                $model->attributes = $_POST['ContactForm'];
                $error='';
                $success='';
                if($model->validate()){
                    $success = 'Письмо отправлено. В течении суток Вам ответят.';
                    $message = $this->renderPartial('feedbackMail', array('model'=>$model), true);
                    Qwe::model()->mail('TMake.ru: Обратная связь', $model->email, $message);
                }else{
                    $error = '<ul>';
                    foreach($model->getErrors() as $i=>$unit)
                        $error .= '<li>'.$unit[0].'</li>';
                    $error .= '</ul>';
                }
                
                echo json_encode(array(
                    'success'=>$success,
                    'error'=>$error
                ));
            }
            Yii::app()->end();
        }
        $this->redirect('/price');
    }
    
    /**
     * Контакты
     */
    public function actionContacts(){
        $this->render('contacts');
    }
    
    /**
     * Rss канал
     */
    public function actionRss()
    {
        Yii::import('ext.feed.*');
        // RSS 2.0 is the default type
        $feed = new EFeed(EFeed::RSS1);
        
        $criteria = new CDbCriteria;
        $criteria->limit = 5;
        $criteria->order = 'date_create';
        $blog = Blog::model()->with('blogCategories')->findAll($criteria);
        
        $this->renderPartial('rss',array(
            'feed'=>$feed,
            'blog'=>$blog
        ));
    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}