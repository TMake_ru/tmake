<?php

class BlogController extends Controller
{
    public $layout = '//layouts/column2';
    
    public function actions(){
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
            ),
        );
    }
    
    /**
     * Список блогов
     */
    public function actionIndex(){
        $criteria = new CDbCriteria;
        $criteria->addCondition('status=:stat');
        $criteria->params[':stat'] = Blog::STAT_PUB;
        $criteria->order = 'date_create DESC';
        $count = Blog::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 4;
        $pages->applyLimit($criteria);
        $model = Blog::model()->findAll($criteria);
        
        $this->render('index',array(
            'model'=>$model,
            'pages'=>$pages
        ));
    }
    
    /**
     * Просмотр блога
     * @param string $alias
     */
    public function actionView($alias)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('alias=:alias');
        $criteria->params[':alias'] = $alias;
        $criteria->order = 'blogComments.date_create';
        $model = Blog::model()->with('blogCategories','blogComments')->find($criteria);
        if($model == NULL)
            $this->redirect('/blog', true, 301);
        
        if(IpControl::model()->isAccess(IpControl::TYPE_VIEW_BLOG, $model->id)){
            IpControl::model()->addAccess(IpControl::TYPE_VIEW_BLOG, $model->id);
            $model->count_view += 1;
            $model->save();
        }
        
        $comment = new BlogComments;
        $comment->scenario = 'frontend';
        if(!empty($_POST['BlogComments'])){
            $comment->attributes = $_POST['BlogComments'];
            $comment->ip = $_SERVER['REMOTE_ADDR'];
            $comment->blog_id = $model->id;
            $comment->date_create = date('Y-m-d H:i:s');
            $comment->rating = 0;
            if($comment->save()){
                $model->count_comment = BlogComments::model()->countByAttributes(array('blog_id'=>$model->id));
                $model->save();
                $this->redirect('/blog/'.CHtml::encode($alias).'#list-comment');
            }
        }
        
        $this->render('view',array(
            'model'=>$model,
            'comment'=>$comment,
        ));
    }
    
    /**
     * Изменение рейтинга
     */
    public function actionRating(){
        if(!empty($_GET['type']) && !empty($_GET['id']) && !empty($_GET['status'])){
            $ipControl = IpControl::model()->addAccess($_GET['type'], $_GET['id']);
            switch($_GET['type']){
                case IpControl::TYPE_BLOG: 
                    $this->ratingBlog($_GET['id'], $_GET['status'], $ipControl);
                    break;
                
                case IpControl::TYPE_COMM_BLOG: 
                    $this->ratingCommentBlog($_GET['id'], $_GET['status'], $ipControl);
                    break;
            }
        }
        $this->redirect('/blog');
    }
    
    /**
     * Изменение рейтинга блога
     * @param int $id
     * @param string $status
     * @param boolean $ipControl
     */
    private function ratingBlog($id, $status, $ipControl)
    {
        $model = Blog::model()->findByPk($id);
        if($model != NULL){
            if($ipControl){
                if($status == 'down')
                    $model->count_dislike += 1;
                else
                    $model->count_like += 1;
                
                $model->save();
                //var_dump($model->getErrors()); exit;
            }
            $this->redirect('/blog/'.CHtml::encode($model->alias));
        }
    }
    
    /*
     * Изменение рейтинга комментария к блогу
     */
    private function ratingCommentBlog($id, $status, $ipControl){
        $model = BlogComments::model()->findByPk($id);
        if($model != NULL){
            if($ipControl){
                $model->rating = ($status == 'down')? $model->rating - 1: $model->rating + 1;
                $model->save();
            }
            $this->redirect('/blog/'.CHtml::encode(Blog::model()->getField($model->blog_id, 'alias')).'#list-comment');
        }
    }
}
