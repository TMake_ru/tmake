<?php

class PortfolioController extends Controller
{
    public $layout = '//layouts/column2';
    
    public function actions(){
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
            ),
        );
    }
    
    /**
     * Список работ
     */
    public function actionIndex()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'date_create DESC';
        $count = Portfolio::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 8;
        $pages->applyLimit($criteria);
        $model = Portfolio::model()->findAll($criteria);
        
        $this->render('index',array(
            'model'=>$model,
            'pages'=>$pages
        ));
    }
    
    /**
     * Просмотр работы
     */
    public function actionView($alias)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('alias=:alias');
        $criteria->params[':alias'] = $alias;
        $criteria->order = 'prtfComments.date_create';
        $model = Portfolio::model()->with('prtfComments')->find($criteria);
        
        if($model == NULL)
            $this->redirect('/portfolio',true,301);
        
        if(IpControl::model()->isAccess(IpControl::TYPE_VIEW_PRTF, $model->id)){
            $model->count_view += 1;
            $model->save();
            IpControl::model()->addAccess(IpControl::TYPE_VIEW_PRTF, $model->id);
        }
        
        // Галерея
        $criteria = new CDbCriteria;
        $criteria->addCondition('prtf_id=:pid');
        $criteria->params[':pid'] = $model->id;
        $criteria->order = 'position';
        $gallery = PrtfGallery::model()->findAll($criteria);
        
        $criteria = new CDbCriteria;
        $criteria->addCondition('prtf_id=:pid');
        $criteria->params[':pid'] = $model->id;
        $criteria->order = 'position';
        $code = PrtfCode::model()->findAll($criteria);
        
        $comment = new PrtfComments;
        $comment->scenario = 'frontend';
        if(!empty($_POST['PrtfComments'])){
            $comment->attributes = $_POST['PrtfComments'];
            $comment->prtf_id = $model->id;
            $comment->date_create = date('Y-m-d H:i:s');
            $comment->rating = 0;
            $comment->ip = $_SERVER['REMOTE_ADDR'];
            if($comment->save()){
                $model->count_comment = PrtfComments::model()->countByAttributes(array('prtf_id'=>$model->id));
                $model->save();
                $this->redirect('/portfolio/'.CHtml::encode($alias).'#list-comment');
            }
        }
        
        $is = IpControl::model()->isAccess(IpControl::TYPE_PRTF, $model->id);
        
        $this->render('view',array(
            'model'=>$model,
            'gallery'=>$gallery,
            'code'=>$code,
            'comment'=>$comment,
            'is'=>$is,
        ));
    }
    
    /**
     * Изменение рейтинга
     */
    public function actionRating(){
        if(!empty($_GET['type']) && !empty($_GET['id']) && !empty($_GET['status'])){
            $ipControl = IpControl::model()->addAccess($_GET['type'], $_GET['id']);
            switch($_GET['type']){
                case IpControl::TYPE_PRTF: 
                    $this->ratingPrtf($_GET['id'], $_GET['status'], $ipControl);
                    break;
                
                case IpControl::TYPE_COMM_PRTF: 
                    $this->ratingCommentPrtf($_GET['id'], $_GET['status'], $ipControl);
                    break;
            }
        }
        $this->redirect('/blog');
    }
    
    /**
     * Изменение рейтинга блога
     * @param int $id
     * @param string $status
     * @param boolean $ipControl
     */
    private function ratingPrtf($id, $status, $ipControl)
    {
        $model = Portfolio::model()->findByPk($id);
        if($model != NULL){
            if($ipControl){
                if($status == 'down')
                    $model->count_dislike += 1;
                else
                    $model->count_like += 1;
                
                $model->save();
                //var_dump($model->getErrors()); exit;
            }
            $this->redirect('/portfolio/'.CHtml::encode($model->alias));
        }
    }
    
    /*
     * Изменение рейтинга комментария к блогу
     */
    private function ratingCommentPrtf($id, $status, $ipControl){
        $model = PrtfComments::model()->findByPk($id);
        if($model != NULL){
            if($ipControl){
                $model->rating = ($status == 'down')? $model->rating - 1: $model->rating + 1;
                $model->save();
            }
            $this->redirect('/portfolio/'.CHtml::encode(Portfolio::model()->getField($model->prtf_id, 'alias')).'#list-comment');
        }
    }
}
