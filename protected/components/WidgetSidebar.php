<?php

class WidgetSidebar extends Portlet{
    
    public function renderContent(){
        $category = Category::model()->findAll(array('order'=>'title_name'));
        
        $criteria = new CDbCriteria;
        $criteria->addCondition('status=:status');
        $criteria->params[':status'] = Blog::STAT_PUB;
        $criteria->order = 'count_like DESC';
        $criteria->limit = 2;
        $blog = Blog::model()->findAll($criteria);
        
        $this->render('widget/sidebar',array(
            'category'=>$category,
            'blog'=>$blog
        ));
    }
}
