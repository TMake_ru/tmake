<?php

class WidgetTop extends Portlet{
    
    public function renderContent() {
        $this->render('widget/top',array(
            'controller'=>Yii::app()->controller->id,
            'action'=>Yii::app()->controller->action->id
        ));
    }
}