<?php

class WidgetFeedback extends Portlet
{
    public $header = 'Связаться со мной';
    
    public function renderContent() {
        $form = new ContactForm;
        if(!empty($_POST['ContactForm'])){
            $form->attributes = $_POST['ContactForm'];
            if($form->validate()){
                $message = $this->renderPartial('feedback/mail', array('model'=>$form), true);
                if(Qwe::model()->mail('TMake.ru: Обратная связь', $form->email, $message)){
                    Yii::app()->user->setFlash('feedback','Письмо отправлено. В течении суток Вам ответят.');
                    $this->refresh();
                }
            }
        }
        
        $this->render('feedback/form', array('form'=>$form, 'header'=>$this->header));
    }
}
