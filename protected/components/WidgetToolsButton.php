<?php

class WidgetToolsButton extends Portlet
{
    public $tools;
    public $layout = '';
    
    public function renderContent() {
        if(get_class($this->tools) == 'Portfolio'){
            $type = IpControl::TYPE_PRTF;
            $url = '/portfolio/rating';
        }
        else{
            $type = IpControl::TYPE_BLOG;
            $url = '/blog/rating';
        }
        $is = IpControl::model()->isAccess($type, $this->tools->id);
        
        $this->render('widget/toolsButton'.$this->layout,array(
            'unit'=>$this->tools,
            'is'=>$is,
            'url'=>$url,
            'type'=>$type,
        ));
    }
}
