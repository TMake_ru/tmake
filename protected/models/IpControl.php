<?php

/**
 * This is the model class for table "ip_control".
 *
 * The followings are the available columns in table 'ip_control':
 * @property integer $id
 * @property integer $data_id
 * @property string $ip
 * @property string $date_create
 * @property string $type
 */
class IpControl extends CActiveRecord
{
    const TYPE_BLOG = 'blog';
    const TYPE_COMM_BLOG = 'commentBlog';
    const TYPE_VIEW_BLOG = 'viewBlog';
    const TYPE_PRTF = 'portfolio';
    const TYPE_COMM_PRTF = 'commentPortfolio';
    const TYPE_VIEW_PRTF = 'viewPortfolio';
    
    private $_type = array(
        self::TYPE_BLOG => 'Блог',
        self::TYPE_COMM_BLOG => 'Комментарий к блоги',
        self::TYPE_VIEW_BLOG => 'Просмотр блога',
        self::TYPE_PRTF => 'Портфолио',
        self::TYPE_COMM_PRTF => 'Комментарий к портфолио',
        self::TYPE_VIEW_PRTF => 'Просмотр портфолио',
    );
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ip_control';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_id, ip, type', 'required'),
			array('data_id', 'numerical', 'integerOnly'=>true),
			array('ip', 'length', 'max'=>150),
			array('type', 'length', 'max'=>16),
            array('date_create','length'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data_id, ip, date_create, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return array(
			'id' => 'ID',
			'data_id' => 'Принадлежность',
			'ip' => 'Ip',
			'date_create' => 'Дата добавления',
			'type' => 'Тип',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data_id',$this->data_id);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('date_create',$this->date_create,true);
		$criteria->compare('type',$this->type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return IpControl the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    /**
     * Наименование типа
     * @param string $num
     * @return string
     */
    public function getType($num){
        return !empty($this->_type[$num])? $this->_type[$num]: Yii::app()->params['empty'];
    }
    
    /**
     * Массив наименование типов
     * @param boolean $null
     * @param array $array
     * @return array
     */
    public function getTypeAll($null=false, $array=array(''=>'')){
        return Qwe::model()->arrayConst($this->_type, $null, $array);
    }
    
    /**
     * Наименивание принадлежности
     * @param int $id
     * @param string $type
     */
    public function getNameData($id, $type, $array=false, $null=false, $first = array(''=>'')){
        $res = ($array === false)? Qwe::t('Undefined'): array();
        switch($type){
            case self::TYPE_BLOG:
                $res = ($array === false)? Blog::model()->getField($id): Blog::model()->getFieldAll($null, $first);
                break;
            
            case self::TYPE_VIEW_BLOG:
                $res = ($array === false)? Blog::model()->getField($id): Blog::model()->getFieldAll($null, $first);
                break;
            
            case self::TYPE_COMM_BLOG:
                $res = ($array === false)? BlogComments::model()->getField($id): BlogComments::model()->getFieldAll($null, $first);
                break;
            
            case self::TYPE_PRTF:
                $res = ($array === false)? Portfolio::model()->getField($id): Portfolio::model()->getFieldAll($null, $first);
                break;
            
            case self::TYPE_VIEW_PRTF:
                $res = ($array === false)? Portfolio::model()->getField($id): Portfolio::model()->getFieldAll($null, $first);
                break;
            
            case self::TYPE_COMM_PRTF:
                $res = ($array === false)? PrtfComments::model()->getField($id): PrtfComments::model()->getFieldAll($null, $first);
                break;
        }
        return $res;
    }
    
    /**
     * Проверяем доступ
     * @param string $type
     * @param int $data_id
     * @return boolean
     */
    public function isAccess($type, $data_id){
        return ($this->countByAttributes(array('type'=>$type,'data_id'=>$data_id, 'ip'=>$_SERVER['REMOTE_ADDR'])) == 0)? true: false;
    }
    
    /**
     * Добавляем метку
     * @param string $type
     * @param int $data_id
     * @return boolean
     */
    public function addAccess($type, $data_id){
        if($this->isAccess($type, $data_id)){
            $model = new self;
            $model->date_create = date('Y-m-d H:i:s');
            $model->ip = $_SERVER['REMOTE_ADDR'];
            $model->data_id = $data_id;
            $model->type = $type;
            return $model->save();
        }
        return false;
    }
}
