<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $id
 * @property string $title_name
 * @property string $alias
 * @property integer $count_blog
 *
 * The followings are the available model relations:
 * @property BlogCategory[] $blogCategories
 */
class Category extends CActiveRecord
{
    public $blogId;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title_name, alias', 'required'),
			array('count_blog', 'numerical', 'integerOnly'=>true),
			array('title_name', 'length', 'max'=>256),
			array('alias', 'length', 'max'=>255),
            array('alias', 'unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title_name, alias, count_blog', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'blogCategories' => array(self::HAS_MANY, 'BlogCategory', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title_name' => 'Название',
			'alias' => 'Alias',
			'count_blog' => 'Количество блогов',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
		$criteria->compare('title_name',$this->title_name,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('count_blog',$this->count_blog);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    /**
     * Выборка поля
     * @param int $id
     * @param string $field Default to 'title_name'
     * @return string
     */
    public function getField($id, $field='title_name'){
        $model = $this->findByPk($id);
        return ($model != NULL)? $model->{$field}: Qwe::t('Empty');
    }
    
    /**
     * Массив выборки
     * @param boolean $null Default to FALSE
     * @param array $array Default to array(''=>'')
     * @param string $key Default to 'id'
     * @param string $val Default to 'title_name'
     * @return array
     */
    public function getFieldAll($null=false, $array=array(''=>''), $key='id', $val='title_name'){
        return Qwe::model()->arrayModel($this, $key, $val, $null, $array);
    }
    
    /**
     * Пред удаление
     * @return boolean
     */
    protected function beforeDelete(){
        if(parent::beforeDelete()){
            BlogCategory::model()->deleteAllByAttributes(array('category_id'=>$this->id));
            return true;
        }
        return false;
    }
    
    public function getUrl($catId=false, $not=false){
        if(!$catId){
            $cat = '';
            $and = '?';
        }
        else{
            if($not){
                $cat = '';
                $and = '?';
            }
            else{
                $cat = '?category[]='.CHtml::encode($catId);
                $and = '&';
            }
        }
        
        if(!empty($_GET['category']))
            foreach($_GET['category'] as $unit){
                if($catId != $unit){
                    $cat .= $and.'category[]='.CHtml::encode($unit);
                    $and = '&';
                }
            }
        return $cat;
    }
}
