<?php

class Qwe
{
    public static $_inst = false;
    public static $_lang = array('Empty'=>'Пусто','Undefined'=>'Неопределено');
    
    /**
     * Singleton
     * @return object
     */
    public static function model(){
        if(self::$_inst === false)
            self::$_inst = new self;
        return self::$_inst;
    }
    
    /**
     * 
     * @param $mess
     * @return string
     */
    public static function t($mess){
        return (Yii::app()->language == 'ru')? self::$_lang[$mess]: $mess;
    }
    
    /**
     * Массив из переменной с пред строками
     * @param array $const
     * @param boolean $null Default to FALSE
     * @param array $array default to array(''=>'')
     * @return array
     */
    public function arrayConst(&$const, $null=false, $array=array(''=>'')){
        if($null){
            foreach($const as $k=>$v)
                $array[$k] = $v;
            return $array;
        }
        return $const;
    }
    
    /**
     * Массив из выборки с пред строками
     * @param object $obj
     * @param string $key
     * @param string $val
     * @param boolean $null Default to FALSE
     * @param array $array default to array(''=>'')
     * @param false||object $criteria Default to FALSE
     * @return array
     */
    public function arrayModel(&$obj, $key, $val, $null=false, $array=array(''=>''), $criteria=false){
        $model = ($criteria === false)? $obj->findAll(): $obj->findAll($criteria);
        $result = ($null === false)? array(): $array;
        if(count($model)>0)
            foreach($model as $unit)
                $result[$unit->{$key}] = $unit->{$val};
        return $result;
    }
    
    /**
     * Формат даты
     * @param date $date
     * @param string $format Default to 'd.m.Y'
     * @return string
     */
    public static function datef($date, $format='d.m.Y'){
        $dt = new DateTime($date);
        return $dt->format($format);
    }
    
    /**
     * Проверка на существование файла
     * @param stinrg $dir_file
     * @return boolean
     */
    public function isFile($dir_file){
        return (is_file(Yii::getPathOfAlias('webroot').$dir_file) && file_exists(Yii::getPathOfAlias('webroot').$dir_file))? true: false;
    }
    
    /**
     * Отправка почты
     * @param string $subject
     * @param string $to
     * @param string $message
     * @return boolean
     */
    public function mail($subject, $to, $message)
    {     
        $to = Yii::app()->params['adminEmail'];
        $from = Yii::app()->params['feedbackEmail'];
        $subject = '=?utf-8?b?'. base64_encode($subject) .'?='; 
        $headers = "Content-type: text/html; charset=\"utf-8\"\r\n"; 
        $headers .= "From: <". $from .">\r\n"; 
        $headers .= "MIME-Version: 1.0\r\n"; 
        $headers .= "Date: ". date('D, d M Y h:i:s O') ."\r\n"; 

       return mail($to, $subject, $message, $headers);
    }
    
    /**
     * Короткий url
     * @param string $url
     * @return string
     */
    public static function shortUrl($url){
        return preg_replace('#http:\/\/www\.|http:\/\/|/.*#', '', $url);
    }
}
