<?php

/**
 * This is the model class for table "blog".
 *
 * The followings are the available columns in table 'blog':
 * @property integer $id
 * @property string $header
 * @property string $keywords
 * @property string $description
 * @property string $alias
 * @property string $short_description
 * @property string $text_description
 * @property string $date_create
 * @property string $preview
 * @property integer $count_view
 * @property integer $count_like
 * @property integer $count_dislike
 * @property integer $count_comment
 * @property string $status
 *
 * The followings are the available model relations:
 * @property BlogCategory[] $blogCategories
 * @property BlogComments[] $blogComments
 */
class Blog extends CActiveRecord
{
    const STAT_PUB = 'public';
    const STAT_PRI = 'private';
    const DIR_FILE = '/uploads/blog/';
    
    private $_status = array(
        self::STAT_PUB => 'Опубликовано',
        self::STAT_PRI => 'Закрыто',
    );
    private $_preview;
    
    public $upload_preview;
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'blog';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('header, alias, short_description, text_description, status', 'required'),
			array('count_view, count_like, count_dislike, count_comment', 'numerical', 'integerOnly'=>true),
			array('header, keywords', 'length', 'max'=>256),
			array('description', 'length', 'max'=>400),
			array('alias', 'length', 'max'=>255),
			array('preview', 'length', 'max'=>150),
			array('status', 'length', 'max'=>7),
            array('alias','unique'),
            array('upload_preview','file','types'=>'jpeg,jpg,png,gif','allowEmpty'=>true),
            array('date_create','length'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, header, keywords, description, alias, short_description, text_description, date_create, preview, count_view, count_like, count_dislike, count_comment, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'blogCategories' => array(self::HAS_MANY, 'BlogCategory', 'blog_id'),
			'blogComments' => array(self::HAS_MANY, 'BlogComments', 'blog_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'header' => 'Загаловок',
			'keywords' => 'Meta-Keywords',
			'description' => 'Meta-Description',
			'alias' => 'Alias',
			'short_description' => 'Короткий текст',
			'text_description' => 'Основной текст',
			'date_create' => 'Дата добавления',
			'preview' => 'Изображение',
            'upload_preview' => 'Изображение',
			'count_view' => 'Просмотров',
			'count_like' => 'Нравится',
			'count_dislike' => 'Не нравится',
			'count_comment' => 'Комментариев',
			'status' => 'Статус',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('header',$this->header,true);
		$criteria->compare('keywords',$this->keywords,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('short_description',$this->short_description,true);
		$criteria->compare('text_description',$this->text_description,true);
		$criteria->compare('date_create',$this->date_create,true);
		$criteria->compare('preview',$this->preview,true);
		$criteria->compare('count_view',$this->count_view);
		$criteria->compare('count_like',$this->count_like);
		$criteria->compare('count_dislike',$this->count_dislike);
		$criteria->compare('count_comment',$this->count_comment);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Blog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    /**
     * Выборка строки
     * @param int $id
     * @param string $field
     * @return 
     */
    public function getField($id, $field = 'header'){
        $model = $this->findByPk($id);
        return ($model != NULL)? $model->{$field}: Qwe::t('Empty');
    }
    
    /**
     * Массив выбранных строк
     * @param boolean $null Default to False
     * @param array $array Default to array(''=>'')
     * @param string $key Default to 'id'
     * @param string $value Default to 'header'
     * @return array
     */
    public function getFieldAll($null=false, $array=array(''=>''), $key='id', $value='header'){
        return Qwe::model()->arrayModel($this, $key, $value, $null, $array);
    }
    
    /**
     * Наименование статуса
     * @param string $num
     * @return string
     */
    public function getStatus($num){
        return !empty($this->_status[$num])? $this->_status[$num]: Qwe::t('Empty');
    }
    
    /**
     * Массив наименований статуса
     * @param boolean $null Default to FALSE
     * @param array $array Default to array(''=>'')
     * @return array
     */
    public function getStatusAll($null=false, $array=array(''=>'')){
        return Qwe::model()->arrayConst($this->_status, $null, $array);
    }
    
    /**
     * Пред сохранение
     * @return boolean
     */
    protected function beforeSave(){
        if(parent::beforeSave()){
            $this->_preview = $this->preview;
            $this->upload_preview = CUploadedFile::getInstance($this, 'upload_preview');
            if($this->upload_preview != NULL)
                $this->preview = UploadImage::model()->getNewName().'.'.$this->upload_preview->getExtensionName();
            return true;
        }
        return false;
    }


    /**
     * Пост сохранение
     * @return parent::afterSave();
     */
    protected function afterSave(){
        if($this->upload_preview != NULL){
            $this->upload_preview->saveAs(Yii::getPathOfAlias('webroot').self::DIR_FILE.$this->preview);
            UploadImage::model()->deleteFile(self::DIR_FILE.$this->_preview);
        }
        Imperavi::model()->CheckImperavi($this);
        return parent::afterSave();
    }
    
    /**
     * Пред удаление
     * @return boolean
     */
    protected function beforeDelete(){
        if(parent::beforeDelete()){
            UploadImage::model()->deleteFile(self::DIR_FILE.$this->preview);
            BlogCategory::model()->deleteAllByAttributes(array('blog_id'=>$this->id));
            BlogComments::model()->deleteAllByAttributes(array('blog_id'=>$this->id));
            Imperavi::model()->deleteImage($this);
            return true;
        }
        return false;
    }
}
