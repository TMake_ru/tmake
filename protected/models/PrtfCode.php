<?php

/**
 * This is the model class for table "prtf_code".
 *
 * The followings are the available columns in table 'prtf_code':
 * @property integer $id
 * @property integer $prtf_id
 * @property string $header
 * @property string $code_description
 * @property integer $position
 *
 * The followings are the available model relations:
 * @property Portfolio $prtf
 */
class PrtfCode extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'prtf_code';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prtf_id, header, code_description', 'required'),
			array('prtf_id, position', 'numerical', 'integerOnly'=>true),
			array('header', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, prtf_id, header, code_description, position', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prtf' => array(self::BELONGS_TO, 'Portfolio', 'prtf_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'prtf_id' => 'Портфолио',
			'header' => 'Заголовок',
			'code_description' => 'Код для примера',
			'position' => 'Позиция',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('prtf_id',$this->prtf_id);
		$criteria->compare('header',$this->header,true);
		$criteria->compare('code_description',$this->code_description,true);
		$criteria->compare('position',$this->position);
        $criteria->order = 'position';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PrtfCode the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    /**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PrtfCode the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=$this->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}
