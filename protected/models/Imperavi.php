<?php

/**
 * This is the model class for table "imperavi".
 *
 * The followings are the available columns in table 'imperavi':
 * @property integer $id
 * @property string $image
 */
class Imperavi extends CActiveRecord
{
    const DIR_IMPERAVI = '/uploads/imperavi/';
    const STAT_TRUE = 'true';
    const STAT_FALSE = 'false';
  
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'imperavi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('image', 'required'),
			array('modelId', 'numerical', 'integerOnly'=>true),
			array('image, modelName, field', 'length', 'max'=>255),
            array('status','length','max'=>8),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'image' => 'Image',
            'status' => 'Status',
            'modelName'=> 'Model Name',
            'field'=>'field',
            'modelId' => 'Model Id',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('image',$this->image,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Imperavi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
  
    /**
     * Проверка изображения из imperavi
     * @param object $obj
     * @param array $fields
     */
    public function CheckImperavi($obj,$fields=array('text_description')){

      foreach($fields as $field)
      {
        $fileContent = $this->getImageInContent($obj,$field);
        $criteria = new CDbCriteria;
        if($obj->scenario == 'adminUpdate')
        {  
          $criteria->addCondition('modelName=:modelName AND modelId=:modelId AND status=:status AND field=:field');
          $criteria->params[':modelName'] = get_class($obj);
          $criteria->params[':modelId'] = $obj->id;
          $criteria->params[':status'] = self::STAT_TRUE;
          $criteria->params[':field'] = $field;
          $model = $this->findAll($criteria);

          $modelArray = CHtml::listData($model, 'image', 'image');
          $modelDiff = array_diff($modelArray,$fileContent);

          if(count($modelDiff)>0)
          {
              foreach($modelDiff as $file)
                UploadImage::model()->deleteFile(self::DIR_IMPERAVI.$file);
            $criteria = new CDbCriteria;
            $criteria->addInCondition('image', $modelDiff);
            $this->deleteAll($criteria);
          }
        }
        elseif($obj->scenario == 'adminCreate')
        {
          $criteria = new CDbCriteria;
          $criteria->addCondition('status=:status AND modelName=:modelName AND field=:field');
          $criteria->addInCondition('image', $fileContent);
          $criteria->params[':status'] = self::STAT_FALSE;
          $criteria->params[':modelName'] = get_class($obj);
          $criteria->params[':field'] = $field;
          $model = $this->findAll($criteria);

          if(count($model)>0)
            foreach($model as $unit)
            {
              $item = $this->findByPk($unit->id);
              $item->status = self::STAT_TRUE;
              $item->modelId = $obj->id;
              $item->save();
            }
        }

        $this->deleteImage($obj, $field);
      }
    }

    /**
     * Выборка изображения из контента
     * @param object $obj
     * @param string $field
     * @return array
     */
    public function getImageInContent($obj,$field){
        preg_match_all('#<img\ssrc="([^"]*)"#iUm', $obj->{$field},$match);
        $array = array();
        if(!empty($match[1]))
          if(count($match[1])>0)
            foreach($match[1] as $unit){
              $array[] = end(explode('/',$unit));
            }
        return $array;
    }

    /**
     * Удаление изображений
     * @param object $obj
     * @param string|false $field
     */
    public function deleteImage(&$obj, $field=false){
      $criteria = new CDbCriteria;
      if($field)
        $criteria->addCondition('modelName=:modelName AND modelId=:modelId AND status=:status AND field=:field');
      else
        $criteria->addCondition('modelName=:modelName AND modelId=:modelId');

      $criteria->params[':modelName'] = get_class($obj);
      $criteria->params[':modelId'] = $obj->id;
      if($field){
        $criteria->params[':status'] = self::STAT_FALSE;
        $criteria->params[':field'] = $field;
      }
      $model = $this->findAll($criteria);

      if(count($model)>0)
        foreach($model as $unit)
          UploadImage::model()->deleteFile(self::DIR_IMPERAVI.$unit->image);
      $this->deleteAll($criteria);
    }
}
