<?php

/**
 * This is the model class for table "blog_category".
 *
 * The followings are the available columns in table 'blog_category':
 * @property integer $id
 * @property integer $blog_id
 * @property integer $category_id
 *
 * The followings are the available model relations:
 * @property Category $category
 * @property Blog $blog
 */
class BlogCategory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'blog_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('blog_id, category_id', 'required'),
			array('blog_id, category_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, blog_id, category_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'blog' => array(self::BELONGS_TO, 'Blog', 'blog_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'blog_id' => 'Блог',
			'category_id' => 'Категория',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('blog_id',$this->blog_id);
		$criteria->compare('category_id',$this->category_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BlogCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    /**
     * Пост сохранение
     * @return void
     */
    protected function afterSave(){
        $model = Category::model()->findByPk($this->category_id);
        if($model != NULL){
            $model->count_blog = $this->countByAttributes(array('category_id'=>$this->category_id));
            $model->save();
        }
        return parent::afterSave();
    }
    
    /**
     * Пост удаление
     * @return void
     */
    protected function afterDelete(){
        $model = Category::model()->findByPk($this->category_id);
        if($model != NULL){
            $model->count_blog = $this->countByAttributes(array('category_id'=>$this->category_id));
            $model->save();
        }
        return parent::afterDelete();
    }
}
