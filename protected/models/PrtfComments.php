<?php

/**
 * This is the model class for table "prtf_comments".
 *
 * The followings are the available columns in table 'prtf_comments':
 * @property integer $id
 * @property integer $prtf_id
 * @property string $date_create
 * @property string $text_description
 * @property integer $rating
 *
 * The followings are the available model relations:
 * @property Portfolio $prtf
 */
class PrtfComments extends CActiveRecord
{
    public $verifyCode;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'prtf_comments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prtf_id, username, text_description', 'required'),
			array('prtf_id, rating', 'numerical', 'integerOnly'=>true),
            array('text_description, date_create','length'),
            array('username', 'length', 'max'=>256),
            array('ip', 'length', 'max'=>100),
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'on'=>'frontend'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, prtf_id, date_create, text_description, rating', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prtf' => array(self::BELONGS_TO, 'Portfolio', 'prtf_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'prtf_id' => 'Портфолио',
            'username' => 'Имя',
            'ip' => 'IP',
			'date_create' => 'Дата добавления',
			'text_description' => 'Сообщение',
            'verifyCode' => 'Проверочный код',
			'rating' => 'Рейтинг',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('prtf_id',$this->prtf_id);
		$criteria->compare('date_create',$this->date_create,true);
		$criteria->compare('text_description',$this->text_description,true);
		$criteria->compare('rating',$this->rating);
        $criteria->order = 'date_create DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PrtfComments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    /**
     * Выборка строки
     * @param int $id
     * @param string $field
     * @return 
     */
    public function getField($id, $field = 'text_description'){
        $model = $this->findByPk($id);
        return ($model != NULL)? 
            'Тема: '.Portfolio::model()->getField($model->prtf_id).' ('.$model->{$field}.')': 
            Qwe::t('Empty');
    }
    
    /**
     * Массив выбранных строк
     * @param boolean $null Default to False
     * @param array $array Default to array(''=>'')
     * @param string $key Default to 'id'
     * @param string $value Default to 'header'
     * @return array
     */
    public function getFieldAll($null=false, $array=array(''=>''), $key='id', $value='text_description'){
        return Qwe::model()->arrayModel($this, $key, $value, $null, $array);
    }
}
