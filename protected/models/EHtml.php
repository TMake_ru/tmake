<?php

class EHtml extends CHtml
{
  const DIR_FILE = '/assets/cache/';
  
  public static function image($src,$alt='',$htmlOptions=array())
  {
    Yii::import('application.modules.admin.models.UploadImage');
    if(UploadImage::model()->isFile($src)){
      $nameFile = explode('/',$src);

      if(!empty($htmlOptions['params']))
        $params = Yii::app()->params['size'][$htmlOptions['params']];
      elseif(!empty(Yii::app()->params['size'][$nameFile[count($nameFile)-2]]))
        $params = Yii::app()->params['size'][$nameFile[count($nameFile)-2]];
      else
        $params = Yii::app()->params['size']['AppYiiDefault'];

      $type = (!empty($params['type']))? $params['type']: 'resize';
      $name = $params['width'].'x'.$params['height'].'_'.$type.'_'.end($nameFile);

      if(!UploadImage::model()->isFile(Yii::getPathOfAlias('webroot').self::DIR_FILE.$name))
        UploadImage::model()
              ->genCropFile($src,$params['width'],$params['height'],$type)
              ->save(Yii::getPathOfAlias('webroot').self::DIR_FILE.$name);

      return parent::image(self::DIR_FILE.$name, $alt, $htmlOptions);
    }
    else
      return '';
  }
}
