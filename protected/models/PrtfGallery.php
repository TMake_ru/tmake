<?php

/**
 * This is the model class for table "prtf_gallery".
 *
 * The followings are the available columns in table 'prtf_gallery':
 * @property integer $id
 * @property integer $prtf_id
 * @property string $image
 * @property string $alt
 * @property integer $position
 *
 * The followings are the available model relations:
 * @property Portfolio $prtf
 */
class PrtfGallery extends CActiveRecord
{
    const DIR_FILE = '/uploads/prtfGallery/';
    
    public $upload_image;
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'prtf_gallery';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prtf_id, alt', 'required'),
			array('prtf_id, position', 'numerical', 'integerOnly'=>true),
			array('image', 'length', 'max'=>150),
            array('upload_image', 'file', 'types'=>'jpeg,jpg,png,gif', 'allowEmpty'=>true),
			array('alt', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, prtf_id, image, alt, position', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prtf' => array(self::BELONGS_TO, 'Portfolio', 'prtf_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'prtf_id' => 'Портфолио',
			'image' => 'Изображение',
            'upload_image' => 'Изображение',
			'alt' => 'Альтернативный текст',
			'position' => 'Позиция',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('prtf_id',$this->prtf_id);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('alt',$this->alt,true);
		$criteria->compare('position',$this->position);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PrtfGallery the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    /**
     * Пред сохранение
     * @return boolean
     */
    protected function beforeSave(){
        if(parent::beforeSave()){
            $this->upload_image = CUploadedFile::getInstance($this, 'upload_image');
            if($this->upload_image != NULL)
                $this->image = UploadImage::model()->getNewName().'.'.$this->upload_image->getExtensionName();
            return true;
        }
        return false;
    }
    
    /**
     * Пост сохранение
     * @return void
     */
    protected function afterSave(){
        if($this->upload_image != NULL)
            $this->upload_image->saveAs(Yii::getPathOfAlias('webroot').self::DIR_FILE.$this->image);
        return parent::afterSave();
    }
    
    /**
     * Пред удаление
     * @return boolean
     */
    protected function beforeDelete(){
        if(parent::beforeDelete()){
            UploadImage::model()->deleteFile(self::DIR_FILE.$this->image);
            return true;
        }
        return false;
    }
    
    /**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PrtfGallery the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=$this->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}
