<?php

/**
 * This is the model class for table "ban".
 *
 * The followings are the available columns in table 'ban':
 * @property integer $id
 * @property integer $data_id
 * @property string $ip
 * @property integer $date_create
 * @property string $type
 */
class Ban extends CActiveRecord
{
    const TYPE_BLOG = 'blog';
    const TYPE_PRTF = 'portfolio';
    
    private $_type = array(
        self::TYPE_BLOG => 'Блог',
        self::TYPE_PRTF => 'Портфолио',
    );
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ban';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_id, ip, type', 'required'),
			array('data_id', 'numerical', 'integerOnly'=>true),
			array('ip', 'length', 'max'=>150),
			array('type', 'length', 'max'=>9),
            array('date_create','length'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data_id, ip, date_create, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data_id' => 'Принадлежнасть',
			'ip' => 'IP',
			'date_create' => 'Дата добавления',
			'type' => 'Тип',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data_id',$this->data_id);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('date_create',$this->date_create);
		$criteria->compare('type',$this->type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ban the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    /**
     * Наименование типа
     * @param string $num
     * @return string
     */
    public function getType($num){
        return !empty($this->_type[$num])? $this->_type[$num]: Qwe::t('Empty');
    }
    
    /**
     * Массив наименований типов
     * @param boolean $null Default to FALSE
     * @param array $array Default to array(''=>'')
     * @return array
     */
    public function getTypeAll($null=false, $array=array(''=>'')){
        return Qwe::model()->arrayConst($this->_type, $null, $array);
    }
    
    /**
     * Наименивание принадлежности
     * @param int $id
     * @param string $type
     */
    public function getNameData($id, $type, $array=false, $null=false, $first = array(''=>'')){
        $res = ($array === false)? Qwe::t('Undefined'): array();
        switch($type){
            case self::TYPE_BLOG:
                $res = ($array === false)? Blog::model()->getField($id): Blog::model()->getFieldAll($null, $first);
                break;
            case self::TYPE_PRTF:
                $res = ($array === false)? Portfolio::model()->getField($id): Portfolio::model()->getFieldAll($null, $first);
                break;
        }
        return $res;
    }
}
