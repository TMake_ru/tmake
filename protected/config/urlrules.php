<?php

return array(
    '<action:(search|price|contacts|rss)>'=>'site/<action>',
    
    '<controller:(blog|portfolio)>/index'=>'<controller>/index',
    '<controller:(blog|portfolio)>/<action:(captcha|rating)>'=>'<controller>/<action>',
    '<controller:(blog|portfolio)>/<alias:.*>'=>'<controller>/view',
    
    '<controller:\w+>/<id:\d+>'=>'<controller>/view',
    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
);