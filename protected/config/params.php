<?php

return array(
    // this is used in contact page
    'adminEmail' => 'mail@tmake.ru',
    'feedbackEmail'=>'feedback@tmake.ru',
    'size'=>array(
        'AppYiiDefault'=>array('width'=>100,'height'=>100),
        'portfolio'=>array('width'=>200, 'height'=>165,'type'=>'crop'),
        'portfolio-list'=>array('width'=>320, 'height'=>205,'type'=>'crop'),
        'blog-preview'=>array('width'=>150, 'height'=>150,'type'=>'crop'),
        'blog-view'=>array('width'=>200, 'height'=>200,'type'=>'crop'),
        'prtf-gallery'=>array('width'=>200, 'height'=>200,'type'=>'crop')
    ),
);