<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="/admin">
          <?php echo Yii::app()->name;?> Panele
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
				<img src="/assets/img/menu-toggler.png" alt="" />
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->            
				<!-- BEGIN TOP NAVIGATION MENU -->              
				<ul class="nav pull-right">
					<li class="dropdown user">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<span class="username"><?php echo CHtml::encode(Yii::app()->user->email);?></span>
						<i class="icon-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
              <li>
                <a href="/" target="_blank"><i class="icon-eye-open"></i> Перейти на сайт</a>
              </li>
							<li><a href="<?php echo Yii::app()->createUrl('/admin/default/logout');?>"><i class="icon-lock"></i> Выход</a></li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
				<!-- END TOP NAVIGATION MENU --> 
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->