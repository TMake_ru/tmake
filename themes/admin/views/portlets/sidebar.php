<!-- BEGIN SIDEBAR -->
<div class="page-sidebar nav-collapse collapse">
  <!-- BEGIN SIDEBAR MENU -->        
  <ul class="page-sidebar-menu">
    <li>
      <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
      <div class="sidebar-toggler hidden-phone"></div>
      <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    </li>
    <li>
      <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
        <div class="input-box">
          <a href="javascript:;" class="remove"></a>
        </div>
      <!-- END RESPONSIVE QUICK SEARCH FORM -->
    </li>
    <li class="start <?php echo ShAccess::model()->classOfMenu('default','admin ');?>">
        <a href="<?php echo Yii::app()->createUrl('/admin');?>">
            <i class="icon-home"></i> 
            <span class="title">Панель управления</span>
            <span class="selected"></span>
        </a>
    </li>
    <li class="start <?php echo ShAccess::model()->classOfMenu('blog');?>">
      <a href="<?php echo Yii::app()->createUrl('/admin/blog/admin');?>">
        <i class="icon-user"></i> 
        <span class="title">Блог</span>
        <span class="selected"></span>
      </a>
      <?php if(Yii::app()->controller->id == 'blog')
            $this->widget('zii.widgets.CMenu', array(
                    'items'=>$this->menu,
                    'htmlOptions'=>array('class'=>'sub-menu')
            ));?>
    </li>
    <li class="start <?php echo ShAccess::model()->classOfMenu('category');?>">
      <a href="<?php echo Yii::app()->createUrl('/admin/category/admin');?>">
        <i class="icon-user"></i> 
        <span class="title">Категории</span>
        <span class="selected"></span>
      </a>
      <?php if(Yii::app()->controller->id == 'category')
            $this->widget('zii.widgets.CMenu', array(
                    'items'=>$this->menu,
                    'htmlOptions'=>array('class'=>'sub-menu')
            ));?>
    </li>
    <li class="start <?php echo ShAccess::model()->classOfMenu('portfolio');?>">
      <a href="<?php echo Yii::app()->createUrl('/admin/portfolio/admin');?>">
        <i class="icon-user"></i> 
        <span class="title">Портфолио</span>
        <span class="selected"></span>
      </a>
      <?php if(Yii::app()->controller->id == 'portfolio')
            $this->widget('zii.widgets.CMenu', array(
                    'items'=>$this->menu,
                    'htmlOptions'=>array('class'=>'sub-menu')
            ));?>
    </li>
    <li class="start <?php echo ShAccess::model()->classOfMenu('ban');?>">
      <a href="<?php echo Yii::app()->createUrl('/admin/ban/admin');?>">
        <i class="icon-user"></i> 
        <span class="title">Запрет</span>
        <span class="selected"></span>
      </a>
      <?php if(Yii::app()->controller->id == 'ban')
            $this->widget('zii.widgets.CMenu', array(
                    'items'=>$this->menu,
                    'htmlOptions'=>array('class'=>'sub-menu')
            ));?>
    </li>
    <li class="start <?php echo ShAccess::model()->classOfMenu('ipControl');?>">
      <a href="<?php echo Yii::app()->createUrl('/admin/ipControl/admin');?>">
        <i class="icon-user"></i> 
        <span class="title">Ip Контроль</span>
        <span class="selected"></span>
      </a>
      <?php if(Yii::app()->controller->id == 'ipControl')
            $this->widget('zii.widgets.CMenu', array(
                    'items'=>$this->menu,
                    'htmlOptions'=>array('class'=>'sub-menu')
            ));?>
    </li>
  </ul>
  <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->