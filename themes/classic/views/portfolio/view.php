<?php 
$this->pageTitle = Yii::app()->name.' » Портфолио » '.CHtml::encode($model->header);
$this->breadcrumbs = array(
    'Портфолио'=>array('/portfolio'),
    CHtml::encode($model->header),
);
Yii::app()->clientScript->registerMetaTag('Сайт '.CHtml::encode($model->header), 'description');
Yii::app()->clientScript->registerMetaTag('Сайт '.CHtml::encode($model->header), 'keywords');

Yii::app()->clientScript->registerCssFile('/fancybox/jquery.fancybox-1.3.4.css');
Yii::app()->clientScript->registerScriptFile('/fancybox/jquery.fancybox-1.3.4.js',  CClientScript::POS_END);
Yii::app()->clientScript->registerScript('fancybox-script', "
$(document).ready(function() {
    $('.fancybox').fancybox();
});
");
?>

<div class="prtf-block">
    <h3><?php echo CHtml::encode($model->header);?></h3>
    <div class="prtf-info">
      <ul>
        <li>
          <a href="<?php echo CHtml::encode($model->url);?>" target="_blank">
            <?php echo CHtml::encode(Qwe::shortUrl($model->url));?>
          </a>
        </li>
        <li>
          <i class="glyphicon glyphicon-eye-open icon" data-toggle="tooltip" title="Просмотров"></i>
          <?php echo CHtml::encode($model->count_view);?>
        </li>
        <li>
            <?php if($is):?>
            <a href="<?php echo Yii::app()->createUrl('/portfolio/rating', array('type'=>IpControl::TYPE_PRTF, 'id'=>$model->id, 'status'=>'up'));?>" class="button">
            <?php endif;?>
                <i class="glyphicon glyphicon-thumbs-up icon" data-toggle="tooltip" title="Понравилось"></i>
                <?php echo CHtml::encode($model->count_like);?>
            <?php if($is):?>
            </a>
            <?php endif;?>
        </li>
        <li>
            <?php if($is):?>
            <a href="<?php echo Yii::app()->createUrl('/portfolio/rating', array('type'=>IpControl::TYPE_PRTF, 'id'=>$model->id, 'status'=>'down'));?>" class="button">
            <?php endif;?>
                <i class="glyphicon glyphicon-thumbs-down icon" data-toggle="tooltip" title="Не понравилось"></i>
                <?php echo CHtml::encode($model->count_dislike);?>
            <?php if($is):?>
            </a>
            <?php endif;?>
        </li>
      </ul>
    </div>

    <div class="prtf-description">
      <?php echo $model->text_description;?>
    </div>
    <div class="clearfix"></div>
    
    <?php if(count($gallery)>0): $row=1; $first='active';?>
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <?php foreach($gallery as $unit):?>
        
          <?php if($row == 1) echo '<div class="item '.$first.'">'; $first = '';?>
            <?php if(Qwe::model()->isFile(PrtfGallery::DIR_FILE.$unit->image)):?>
                <a class="fancybox" rel="group" href="<?php echo PrtfGallery::DIR_FILE.$unit->image;?>">
                    <?php echo EHtml::image(PrtfGallery::DIR_FILE.$unit->image, CHtml::encode($unit->alt), array('params'=>'prtf-gallery'));?>
                </a>
            <?php endif;?>
          <?php if($row == 3){ $row=1; echo '<div class="carousel-caption"></div></div>';}else $row++;?>
        <?php endforeach;?>
        <?php if($row != 1) echo '<div class="carousel-caption"></div></div>';?>
      </div>

      <!-- Controls -->
      <div class="gallery-button">
        <a class="button-prev" href="#carousel-example-generic" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="button-next" href="#carousel-example-generic" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>
    </div>
    <?php endif;?>
    <!-- /.Carousel -->

    <?php if(count($code)>0):?>
        <?php foreach($code as $unit):?>
            <div class="code-block">
              <h4><?php echo CHtml::encode($unit->header);?>:</h4>
              <div class="code-frame">
                <div class="code-frame-block">
                    <?php echo $unit->code_description;?>
                </div>
              </div>
            </div>
        <?php endforeach;?>
    <?php endif;?>
    <div class="clearfix"></div><!-- /.Example Code -->
</div>
<div class="social">
    <?php $this->widget('WidgetSocial');?>
</div>
<div class="clearfix"></div>

<hr>

<a name="list-comment"></a>
<h3>Комментарии:</h3>

<?php if(count($model->prtfComments)>0):?>
<?php foreach($model->prtfComments as $unit):?>
    <div class="comment-block">
      <div class="comment-avatar">
        <img src="" alt="">
      </div>
      <div class="comment-info">
        <div class="comment-info-description">
          <div class="comment-date">
              <?php echo CHtml::encode($unit->username);?>
              <i><?php echo CHtml::encode(Qwe::datef($unit->date_create, 'd.m.Y H:i:s'));?></i>
          </div>
          <div class="comment-text">
            <?php echo $unit->text_description?>
          </div>
        </div>
        <div class="comment-rating">
            <?php if(IpControl::model()->isAccess(IpControl::TYPE_COMM_PRTF, $unit->id)):?>
                <a href="<?php echo Yii::app()->createUrl('/portfolio/rating',array('type'=>IpControl::TYPE_COMM_PRTF,'id'=>$unit->id, 'status'=>'up'));?>"><i class="glyphicon glyphicon-arrow-up"></i></a>
                <a href="<?php echo Yii::app()->createUrl('/portfolio/rating',array('type'=>IpControl::TYPE_COMM_PRTF,'id'=>$unit->id, 'status'=>'down'));?>"><i class="glyphicon glyphicon-arrow-down"></i></a>
            <?php endif;?>
            <span><?php echo CHtml::encode($unit->rating);?></span>
        </div>
      </div>
    </div>
<?php endforeach;?>
<?php endif;?>


<a name="form-comment"></a>
<div class="row comment-write">
    <form action="#form-comment" method="POST">
        <div class="col-sm-8">
            <?php echo CHtml::activeTextArea($comment, 'text_description',array('placeholder'=>'Сообщение','class'=>'form-control comment-textarea'));?>
            <span class="error-block error-textarea">
                <?php echo CHtml::error($comment, 'text_description');?>
            </span>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
              <?php echo CHtml::activeTextField($comment, 'username', array('class'=>'form-control', 'placeholder'=>'Имя'));?>
              <span class="error-block error-input error-username">
                <?php echo CHtml::error($comment, 'username');?>
              </span>
          </div>
          <div class="form-group">
            <?php echo CHtml::activeTextField($comment, 'verifyCode', array('placeholder'=>'Проверочный код','class'=>'form-control'));?>
            <span class="error-block error-input error-verify">
                <?php echo CHtml::error($comment, 'verifyCode');?>
            </span>
            <?php $this->widget('CCaptcha',array('clickableImage'=>true,'showRefreshButton'=>false)); ?>
          </div>
        </div>
        <div class="col-sm-offset-10 blog-submit">
          <input type="submit" class="btn btn-primary" value="Отправить">
        </div>
    </form>
</div>
<!-- /.Comment portfolio -->