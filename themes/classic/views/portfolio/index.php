<?php 
$this->pageTitle = 'Портфолио';
$this->breadcrumbs = array(
    'Портфолио'
);
Yii::app()->clientScript->registerMetaTag('Примеры созданныйх сайтов. Портфолио.', 'description');
Yii::app()->clientScript->registerMetaTag('Созданные сайты, портфолио, примеры сайтов.', 'keywords');
?>

<div class="pagination">
    <?php $this->widget('CLinkPager',array('pages'=>$pages,'cssFile'=>'','header'=>''));?>
</div><!-- /.Pagination -->

<?php if(count($model)>0):?>
<?php foreach($model as $unit):?>
    <div class="prtf-list-block">
        <div class="prtf-list-preview">            
            <a href="<?php echo Yii::app()->createUrl('/portfolio/'.CHtml::encode($unit->alias));?>">
                <div class="prtf-list-image">
                    <div class="prtf-list-image-alt">
                        <div class="prtf-alt"><?php echo CHtml::encode($unit->header);?></div>
                        <div class="prtf-background"></div>
                    </div>
                    <?php if(Qwe::model()->isFile(Portfolio::DIR_FILE.$unit->preview)):?>
                        <?php echo EHtml::image(Portfolio::DIR_FILE.$unit->preview, CHtml::encode($unit->header),array('params'=>'portfolio-list'));?>
                    <?php else:?>
                        <?php echo EHtml::image('/images/default.jpg', CHtml::encode($unit->header),array('params'=>'portfolio-list'));?>
                    <?php endif;?>
                </div>
            </a>
          <span class="pull-right">
              <?php $this->widget('WidgetToolsButton', array('tools'=>$unit, 'layout'=>3));?>
          </span>
        </div>
    </div>
<?php endforeach;?>
<?php else:?>
<i>Пусто</i>
<?php endif;?>
<!-- /.Portfilio list -->

<div class="pagination">
    <?php $this->widget('CLinkPager',array('pages'=>$pages,'cssFile'=>'','header'=>''));?>
</div><!-- /.Pagination -->