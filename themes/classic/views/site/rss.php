<?php
$feed->title = Yii::app()->name;
$feed->link = Yii::app()->createAbsoluteUrl('/');
$feed->description = '';
$feed->RSS1ChannelAbout = Yii::app()->createAbsoluteUrl('/rss');

// Blog 
if(count($blog)>0) 
    foreach($blog as $unit){
        $item = $feed->createNewItem();
        $item->title = $unit->header;
        $item->link = Yii::app()->createAbsoluteUrl('/blog/'.CHtml::encode($unit->alias));
        $item->date = CHtml::encode(Qwe::datef($unit->date_create));
        $item->description = $unit->text_description;
        
        if(count($unit->blogCategories))
            foreach($unit->blogCategories as $tag)
                $item->addTag('dc:'.CHtml::encode(Category::model()->getField($tag->category_id, 'alias')), CHtml::encode(Category::model()->getField($tag->category_id)));
        $feed->addItem($item);
    }
 
$feed->generateFeed();
