<?php
$this->pageTitle = Yii::app()->name.' » Услуги';
$this->breadcrumbs = array(
    'Услуги'
);
Yii::app()->clientScript->registerMetaTag('Примерные расценки и сроки для заказа сайта. 
    Разработка сайта под ключ. Доработка существующего сайта.', 'description');
Yii::app()->clientScript->registerMetaTag('Цена на разработку сайта, заказать сайт, техническая поддержка, аудит сайта,', 'keywords');
?>

<div class="price-block">
    <h3>Создание сайта</h3>
    <div class="price-info-short">
        <img alt="Создание сайта"  src="/images/price-image.jpg">
        <ul>
            <li>
                <i class="glyphicon glyphicon-usd"></i>
                от 15 000 руб.
            </li>
            <li>
                <i class="glyphicon glyphicon-calendar"></i>
                от 10 дней
            </li>
        </ul>
    </div>

    <div class="price-info-description">
        Разработка сайта происходит под заказ, без использования шаблонных решений. 
        Работу выполняю в соответствии с техническим заданием, в обговоренные сроки. 
        Сайт способен выдерживать любые нагрузки. Код пишу чистый с техническими комментариями. 
        Проект насыщен паттернами проектирования, что дает возможность передаче сайта сторонним 
        разработчикам на дальнейшую поддержку.
        <div class="proce-button">
            <a href="javascript:void(0);" class="btn btn-primary pull-right" data-toggle="modal" data-target="#orderSite">Заказать сайт</a>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div class="price-block">
    <h3>Сайт под ключ</h3>
    <div class="price-info-short">
        <img alt="Сайт под ключ"  src="/images/price-key.jpeg">
        <ul>
            <li>
                <i class="glyphicon glyphicon-usd"></i>
                от 25 000 руб.
            </li>
            <li>
                <i class="glyphicon glyphicon-calendar"></i>
                от 20 дней
            </li>
        </ul>
    </div>

    <div class="price-info-description">
        Сайт под ключ разрабатывается поэтапно с привлечением сторонних работников смежной специальности: 
        дизайнеров, копирайтеров, контент-менеджеров, seo специалистов. Плюсом такой модели разработки 
        является то, что за рождением вашей идеи можно следить непосредственно в процессе работы. 
        <div class="proce-button">
            <a href="javascript:void(0);" class="btn btn-primary pull-right" data-toggle="modal" data-target="#orderSite">Заказать сайт под ключ</a>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div class="price-block">
    <h3>Помощь в освоении CMS</h3>
    <div class="price-info-short">
        <img alt="Помощь в освоении CMS"  src="/images/price-cms.jpg">
        <ul>
            <li>
                <i class="glyphicon glyphicon-usd"></i>
                от 1 500 руб.
            </li>
            <li>
                <i class="glyphicon glyphicon-calendar"></i>
                от 1 дня
            </li>
        </ul>
    </div>

    <div class="price-info-description">
        Всегда радовали услуги сторонних студий, которые предлагали разработать сайт на CMS, при этом 
        вся разработка заключалась в поиске шаблона и установки CMS. Такой сайт, на самом деле, 
        является бюджетным и может не удовлетворять все потребности владельца. Заказчику нужно быть 
        готовым к тому, что в таком варианте, рано или поздно, появятся ограничения с которыми придется мериться.
        <div class="proce-button">
            <a href="javascript:void(0);" class="btn btn-primary pull-right" data-toggle="modal" data-target="#orderSite">Заказать помощь с CMS</a>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div class="price-block">
    <h3>Аудит сайта</h3>
    <div class="price-info-short">
        <img alt="Аудит сайта"  src="/images/price-audit.png">
        <ul>
            <li>
                <i class="glyphicon glyphicon-usd"></i>
                от 200 руб.
            </li>
            <li>
                <i class="glyphicon glyphicon-calendar"></i>
                от 1 часа
            </li>
        </ul>
    </div>

    <div class="price-info-description">
        Помогу оценить и составить все технические требования к новому сайту или дать подробную 
        характеристику уже существующему сайту. Протестирую сайт на наличие уязвимости и отказоустойчивости. 
        Возможность написание технического задания с разработкой удобного для пользователей интерфейса.
        <div class="proce-button">
            <a href="javascript:void(0);" class="btn btn-primary pull-right" data-toggle="modal" data-target="#orderSite">Заказать аудит сайта</a>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div class="price-block">
    <h3>Техническая поддержка</h3>
    <div class="price-info-short">
        <img alt="Техническая поддержка"  src="/images/price-tp.png">
        <ul>
            <li>
                <i class="glyphicon glyphicon-usd"></i>
                от 6 000 руб.
            </li>
            <li>
                <i class="glyphicon glyphicon-calendar"></i>
                1 месяц
            </li>
        </ul>
    </div>

    <div class="price-info-description">
        Возможны долгосрочные сотрудничества в соответствии с договоренностью. 
        Возможность быть уверенным, что ваш сайт всегда под пристальным вниманием 
        технического специалист. При необходимости реанимирую и предотвращу последующие крушения. 
        Проведение профилактических работ, рефакторинга кода и оптимизация запросов.
        <div class="proce-button">
            <a href="javascript:void(0);" class="btn btn-primary pull-right" data-toggle="modal" data-target="#orderSite">Заказать поддержку сайта</a>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<!-- Modal -->
<div class="modal fade" id="orderSite" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Заказать</h4>
            </div>
            <div class="modal-body">
                <?php $this->renderPartial('feedback',array('model'=>$model));?>
            </div>
        </div>
    </div>
</div>