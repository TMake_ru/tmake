<?php
$this->pageTitle = Yii::app()->name.' » Контакты';
$this->breadcrumbs = array(
    'Контакты'
);
Yii::app()->clientScript->registerMetaTag('Контакты для заказа сайта. Немного о авторе сайта.', 'description');
Yii::app()->clientScript->registerMetaTag('Контакт freelancer, заказать сайт', 'keywords');
?>

<div class="contact-info pull-left">
<ul>
  <li>
    <i class="glyphicon glyphicon-send"></i>
    <a href="mailto:mail@tmake.ru">mail@tmake.ru</a>
  </li>
  <li>
    <i class="glyphicon glyphicon-earphone"></i>
    <a href="skype:+79880984616?call">+7 988 098 46 16</a>
  </li>
  <li>
    <i class="glyphicon glyphicon-phone"></i>
    <a href="skype:Stepan-wbr">Stepan-wbr</a>
  </li>
  <li>
    <i class="glyphicon glyphicon-globe"></i>
    <a href="http://www.linkedin.com/profile/view?id=201433221" target="_blank">Linkedin.com</a>
  </li>
  <li>
    <i class="glyphicon glyphicon-globe"></i>
    <a href="https://twitter.com/TMake_ru" target="_blank">Twitter.com</a>
  </li>
</ul>
</div>
<div class="contact-description pull-left">
<img alt="<?php echo Yii::app()->name;?>" class="example contact-photo" src="https://pbs.twimg.com/profile_images/440399721199198208/z2rvIdc9.jpeg">
<div></div>
<p>Меня зовут Степан.</p>
<p>Разработкой сайтов, я занимаюсь с 2008 года, и за это время проекты были разные, от простейших визиток/vcard, до навороченных социальных сетей/CRM систем.
Разработка сайтов неотъемлемая часть моей жизни и к каждому заказчику подход индивидуальный. При разработке использую Yii framework, но при необходимости не забываю о CMS (Drupal, MODx, Simple и др.).</p>
ДОСТОИНСТВА:
<ul>
    <li>Большой опыт в разработке сайтов.</li>
    <li>Помощь при составлении технического задания.</li>
    <li>Каждый проект выполняется качественно и в срок.</li>
    <li>Для меня нет преград, на выполнения задуманного.</li>
    <li>У постоянных клиентов скидки.</li>
</ul>
НЕДОСТАТКИ
<ul>
    <li>Я один, и не могу делать одновременно несколько проектов.</li>
    <li>Я не занимаюсь дизайном, SEO и копиратингом, но при необходимости могу порекомендовать специалиста.</li>
</ul>
</div>
<div class="clearfix"></div>

<?php $this->widget('WidgetFeedback', array('header'=>'Обратная связь'));?>