<?php Yii::app()->clientScript->registerScript('feedback-script',"
    $('input.submit').click(function(){
        $('.success').addClass('hidden');
        $('.error-feedback').addClass('hidden');
        $.ajax({
            type: 'POST',
            url: '".Yii::app()->createUrl('/site/feedback')."',
            data: $('#feedback-form').serialize(),
            success: function(q){
                res = eval('('+q+')');
                
                if(res['error'] != ''){
                    $('.error-feedback').html(res['error']);
                    $('.error-feedback').removeClass('hidden');
                }
                else if(res['success'] != ''){
                    $('.success').html(res['success']);
                    $('.success').removeClass('hidden');
                    $('#feedback-form')[0].reset();
                }
            }
        });
    });
");?>

<div class="row blog-subscribe">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'feedback-form',
        'enableAjaxValidation'=>false,
    )); ?>
    
        <div class="success hidden"></div>
        <div class="error-feedback hidden"></div>
    
        <div class="col-sm-8">
          <?php echo $form->textArea($model, 'body', array('placeholder'=>'Сообщение', 'class'=>'form-control blog-textarea'));?>
            <span class="error-block error-textarea"><?php echo $form->error($model, 'body');?></span>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <?php echo $form->textField($model, 'username', array('placeholder'=>'Имя', 'class'=>'form-control'));?>
            <span class="error-block error-input error-username"><?php echo $form->error($model, 'username');?></span>
          </div>
          <div class="form-group">
            <?php echo $form->textField($model, 'email', array('placeholder'=>'Email', 'class'=>'form-control'));?>
            <span class="error-block error-input error-email"><?php echo $form->error($model, 'email');?></span>
          </div>
          <div class="form-group">
            <?php echo $form->textField($model, 'phone', array('placeholder'=>'Телефон', 'class'=>'form-control'));?>
            <span class="error-block error-input error-phone"><?php echo $form->error($model, 'phone');?></span>
          </div>
          <div class="form-group">
            <?php echo $form->textField($model, 'verifyCode', array('placeholder'=>'Проверочный код', 'class'=>'form-control'));?>
            <span class="error-block error-input error-verify"><?php echo $form->error($model, 'verifyCode');?></span>
            <?php $this->widget('CCaptcha',array('clickableImage'=>true,'showRefreshButton'=>false));?>
          </div>
        </div>
        <div class="col-sm-offset-8 blog-submit">
          <input type="button" class="btn btn-primary submit" value="Отправить">
        </div>
    <?php $this->endWidget(); ?>
</div><!-- /.subscribe -->