<?php 
$this->pageTitle = Yii::app()->name.' - Поиск';
Yii::app()->clientScript->registerMetaTag('Поиск работ и записей блога freelancer Степан.', 'description');
Yii::app()->clientScript->registerMetaTag('Портфолио, блог, поиск, freelancer.', 'keywords');
?>
<div class="row search-block">
    <form action="<?php echo Yii::app()->createUrl('/search'.Category::model()->getUrl());?>" method="POST">
        <input type="text" class="form-control search-input-full pull-left" placeholder="Поиск" name="s" value="<?php if(!empty($_POST['s'])) echo CHtml::encode($_POST['s']);?>">
        <button type="submit" id="search-button"><i class="glyphicon glyphicon-search"></i></button>
    </form>
</div>

<?php if(!empty($_GET['category'])):?>
<div class="row category-conteiner">
    <?php foreach($category as $unit):?>
    <span class="category-blog">
        <?php echo CHtml::encode($unit->title_name);?>
        <a href="<?php echo Yii::app()->createUrl('/search'.Category::model()->getUrl($unit->alias, true))?>"><span>X</span></a>
    </span>
    <?php endforeach;?>
    <div class="clearfix"></div>
</div>
<?php endif;?>


<?php if(count($prtf)>0 && empty($_GET['category'])):?>
<h2 class="blog-post-title">Из портфолио:</h2>

<?php foreach($prtf as $unit):?>
    <div class="blog-portfolio-preview">
      <div class="blog-portfolio">
        <a href="<?php echo Yii::app()->createUrl('/portfolio/'.CHtml::encode($unit->alias));?>">
            <div class="blog-portfolio-image">
                <div class="blog-portfolio-image-alt">
                    <div class="prtf-alt"><?php echo CHtml::encode($unit->header);?></div>
                    <div class="prtf-background"></div>
                </div>

                <?php if(Qwe::model()->isFile(Portfolio::DIR_FILE.$unit->preview)):?>
                    <?php echo EHtml::image(Portfolio::DIR_FILE.$unit->preview, CHtml::encode($unit->header),array('params'=>'portfolio'));?>
                <?php else:?>
                    <?php echo EHtml::image('/images/default.jpg', CHtml::encode($unit->header),array('params'=>'portfolio'));?>
                <?php endif;?>
            </div>
        </a>
        <span class="pull-right">
            <?php $this->widget('WidgetToolsButton', array('tools'=>$unit));?>
        </span>
      </div>
    </div>
<?php endforeach;?>
<?php endif;?>
<!-- /.portfolio -->

<?php if(count($blog)>0):?>
<div class="row">
  <h2 class="blog-post-title">Из блога:</h2>
  
  <?php foreach($blog as $unit):?>
  <div class="blog-item">
    <span class="pull-left">
      <a href="<?php echo Yii::app()->createUrl('/blog/'.CHtml::encode($unit->alias));?>">
        <?php if(Qwe::model()->isFile(Blog::DIR_FILE.$unit->preview)):?>
          <?php echo EHtml::image(Blog::DIR_FILE.$unit->preview, CHtml::encode($unit->header), array('params'=>'blog-preview'));?>
        <?php endif;?>
      </a>
    </span>
    <div class="pull-right blog-post-block">
      <div class="pull-left">
        <h3>
            <a href="<?php echo Yii::app()->createUrl('/blog/'.CHtml::encode($unit->alias));?>">
                <?php echo CHtml::encode($unit->header);?>
            </a> 
            <small><?php echo CHtml::encode(Qwe::datef($unit->date_create));?></small>
        </h3>
      </div>
      <span class="pull-right">
        <?php $this->widget('WidgetToolsButton', array('tools'=>$unit));?>
      </span>
      <div class="clearfix"></div>
      <p>
        <?php echo CHtml::encode($unit->short_description);?>
      </p>
    </div>
  </div>
  <div class="clearfix"></div>
  <?php endforeach;?>
<!-- /.blog -->
</div>
<?php endif;?>