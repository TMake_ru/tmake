<table>
    <tr>
        <th>Имя пользователя:</th>
        <td><?php echo CHtml::encode($model->username);?></td>
    </tr>
    <tr>
        <th>Email:</th>
        <td><?php echo CHtml::encode($model->email);?></td>
    </tr>
    <tr>
        <th>Телефон:</th>
        <td><?php echo CHtml::encode($model->phone);?></td>
    </tr>
</table>
<hr>
<p>
    <?php echo CHtml::encode($model->body);?>
</p>