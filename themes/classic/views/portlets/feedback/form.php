<a name="feedback"></a>
<div class="row blog-subscribe">
    <form action="#feedback" method="POST">
        <?php if(!empty($header)):?>
        <h2 class="blog-post-title"><?php echo CHtml::encode($header);?>:</h2>
        <?php endif;?>
        <?php if(Yii::app()->user->hasFlash('feedback')): ?>
        <div class="success">
            <?php echo Yii::app()->user->getFlash('feedback');?>
        </div>
        <?php endif;?>
        <div class="col-sm-8">
          <?php echo CHtml::activeTextArea($form, 'body', array('placeholder'=>'Сообщение', 'class'=>'form-control blog-textarea'));?>
            <span class="error-block error-textarea"><?php echo CHtml::error($form, 'body');?></span>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <?php echo CHtml::activeTextField($form, 'username', array('placeholder'=>'Имя', 'class'=>'form-control'));?>
            <span class="error-block error-input error-username"><?php echo CHtml::error($form, 'username');?></span>
          </div>
          <div class="form-group">
            <?php echo CHtml::activeTextField($form, 'email', array('placeholder'=>'Email', 'class'=>'form-control'));?>
            <span class="error-block error-input error-email"><?php echo CHtml::error($form, 'email');?></span>
          </div>
          <div class="form-group">
            <?php echo CHtml::activeTextField($form, 'phone', array('placeholder'=>'Телефон', 'class'=>'form-control'));?>
            <span class="error-block error-input error-phone"><?php echo CHtml::error($form, 'phone');?></span>
          </div>
          <div class="form-group">
            <?php echo CHtml::activeTextField($form, 'verifyCode', array('placeholder'=>'Проверочный код', 'class'=>'form-control'));?>
            <span class="error-block error-input error-verify"><?php echo CHtml::error($form, 'verifyCode');?></span>
            <?php $this->widget('CCaptcha',array('clickableImage'=>true,'showRefreshButton'=>false));?>
          </div>
        </div>
        <div class="col-sm-offset-8 blog-submit">
          <input type="submit" class="btn btn-primary" value="Отправить">
        </div>
    </form>
</div><!-- /.subscribe -->