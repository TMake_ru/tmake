<nav class="blog-nav">
    <a class="blog-nav-item <?php if($controller == 'site' && $action == 'index') echo 'active';?>" href="/">
        Главная
    </a>
    <a class="blog-nav-item <?php if($controller == 'blog') echo 'active';?>" href="<?php echo Yii::app()->createUrl('/blog/index');?>">
        Блог
    </a>
    <a class="blog-nav-item <?php if($controller == 'portfolio') echo 'active';?>" href="<?php echo Yii::app()->createUrl('/portfolio/index');?>">
        Портфолио
    </a>
    <a class="blog-nav-item <?php if($controller == 'site' && $action == 'price') echo 'active';?>" href="<?php echo Yii::app()->createUrl('/price');?>">
        Услуги
    </a>
    <a class="blog-nav-item <?php if($controller == 'site' && $action == 'contacts') echo 'active';?>" href="<?php echo Yii::app()->createUrl('/contacts');?>">
        Контакты
    </a>
    <a class="blog-nav-item <?php if($controller == 'site' && $action == 'rss') echo 'active';?>" href="<?php echo Yii::app()->createUrl('/rss');?>" target="_blank">
        RSS
    </a>
</nav>