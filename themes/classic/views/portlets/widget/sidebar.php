<?php if(!(Yii::app()->controller->id == 'site' && Yii::app()->controller->action->id == 'search')):?>
<div class="form-group blog-search">
    <form action="<?php echo Yii::app()->createUrl('/search'.Category::model()->getUrl());?>" method="GET">
        <input type="text" class="form-control search-input pull-left" placeholder="Поиск" name="s">
        <button type="submit" id="search-button"><i class="glyphicon glyphicon-search"></i></button>
    </form>
</div>
<?php endif;?>

<?php if(count($category)>0):?>
<div class="sidebar-module">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Разделы:</h3>
        </div>
        <ul class="blog-menu list-group">
            <?php foreach($category as $unit):?>
            <li class="list-group-item">
                <a href="<?php echo Yii::app()->createUrl('/search'.Category::model()->getUrl($unit->alias));?>">
                    <?php echo CHtml::encode($unit->title_name);?>
                </a> 
                <small>[<?php echo CHtml::encode($unit->count_blog);?>]</small>
            </li>
            <?php endforeach;?>
        </ul>
    </div>
</div>
<?php endif;?>

<?php if(count($blog)>0):?>
<div class="sidebar-module">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Интересное:</h3>
        </div>
        <div class="panel-body">
            <ul class="blog-menu">
                <?php foreach($blog as $unit):?>
                <li>
                    <h3>
                        <a href="<?php echo Yii::app()->createUrl('/blog/'.CHtml::encode($unit->alias));?>">
                            <?php echo CHtml::encode($unit->header);?>
                        </a> 
                        <small class="pull-right"> <?php echo Qwe::datef($unit->date_create);?></small>
                    </h3>
                    <div>
                        <?php echo CHtml::encode($unit->short_description);?> 
                    </div>
                    <span class="pull-right">
                        <?php $this->widget('WidgetToolsButton', array('tools'=>$unit));?>
                    </span>
                    <div class="clearfix"></div>
                </li>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
</div>
<?php endif;?>

<div class="sidebar-module">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Twitter:</h3>
        </div>
        <div class="panel-body">
            <a class="twitter-timeline" href="https://twitter.com/TMake_ru" data-widget-id="445152796224942080">Твиты пользователя @TMake_ru</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>


        </div>
    </div>
</div>