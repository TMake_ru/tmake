<span class="glyphicon glyphicon-pencil icon" data-toggle="tooltip" title="<?php echo Yii::t('comment-blog','Комментарий|Комментария|Комментариев', $unit->count_comment);?>"><span><?php echo CHtml::encode($unit->count_comment);?></span></span>

<span class="glyphicon glyphicon-thumbs-up icon" data-toggle="tooltip" title="<?php echo Yii::t('like-blog','Понравилось|Понравилось|Понравилось', $unit->count_like);?>"><span><?php echo CHtml::encode($unit->count_like);?></span></span>

<span class="glyphicon glyphicon-thumbs-down icon" data-toggle="tooltip" title="<?php echo Yii::t('dislike-blog','Непонравилось|Непонравилось|Непонравилось', $unit->count_dislike);?>"><span><?php echo CHtml::encode($unit->count_dislike);?></span></span>

<span class="glyphicon glyphicon-eye-open icon" data-toggle="tooltip" title="<?php echo Yii::t('view-blog','Просмотр|Просмотра|Просмотров', $unit->count_view);?>"><span><?php echo CHtml::encode($unit->count_view);?></span></span>