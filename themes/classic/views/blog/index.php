<?php 
$this->pageTitle = Yii::app()->name.' » Блог';
$this->breadcrumbs=array(
	'Блог',
);
Yii::app()->clientScript->registerMetaTag('Как разработать сайт на практике. Список блогов.', 'description');
Yii::app()->clientScript->registerMetaTag('Разработка сайтов, практика разработки сайтов.', 'keywords');
?>
<div class="pagination">
    <?php $this->widget('CLinkPager',array('pages'=>$pages,'cssFile'=>'','header'=>''));?>
</div><!-- /.Pagination -->

<?php if(count($model)>0):?>
<?php foreach($model as $unit):?>
<div class="blog-item">
    <span class="pull-left">
      <a href="<?php echo Yii::app()->createUrl('/blog/'.CHtml::encode($unit->alias));?>">
        <?php if(Qwe::model()->isFile(Blog::DIR_FILE.$unit->preview)):?>
          <?php echo EHtml::image(Blog::DIR_FILE.$unit->preview, CHtml::encode($unit->header), array('params'=>'blog-preview'));?>
        <?php endif;?>
      </a>
    </span>
    <div class="pull-right blog-post-block">
      <div class="pull-left">
        <h3>
            <a href="<?php echo Yii::app()->createUrl('/blog/'.CHtml::encode($unit->alias));?>">
                <?php echo CHtml::encode($unit->header);?>
            </a> 
            <small><?php echo CHtml::encode(Qwe::datef($unit->date_create));?></small>
        </h3>
      </div>
      <span class="pull-right">
        <?php $this->widget('WidgetToolsButton', array('tools'=>$unit));?>
      </span>
      <div class="clearfix"></div>
      <p>
        <?php echo CHtml::encode($unit->short_description);?>
      </p>
    </div>
</div>
<div class="clearfix"></div>
<?php endforeach;?>
<?php else:?>
<i>Пусто</i>
<?php endif;?>

<div class="pagination">
    <?php $this->widget('CLinkPager',array('pages'=>$pages,'cssFile'=>'','header'=>''));?>
</div><!-- /.Pagination -->