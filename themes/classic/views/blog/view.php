<?php 
$this->pageTitle = Yii::app()->name.' » Блог » '.CHtml::encode($model->header);
$this->breadcrumbs=array(
	'Блог'=>array('/blog/index'),
    CHtml::encode($model->header),
);
Yii::app()->clientScript->registerMetaTag(CHtml::encode($model->description), 'description');
Yii::app()->clientScript->registerMetaTag(CHtml::encode($model->keywords), 'keywords');
?>
<div class="post-block">
    <h1><?php echo CHtml::encode($model->header);?></h1>

    <div class="rating-total">
      <?php $this->widget('WidgetToolsButton',array('tools'=>$model,'layout'=>2));?>
    </div>

    <?php if(Qwe::model()->isFile(Blog::DIR_FILE.$model->preview)):?>
    <div class="post-image">
      <?php echo EHtml::image(Blog::DIR_FILE.$model->preview, CHtml::encode($model->header), array('params'=>'blog-view'));?>
    </div>
    <?php endif;?>

    <?php echo $model->text_description;?>
</div>

<div class="post-tags">
    <?php if(count($model->blogCategories)>0) foreach($model->blogCategories as $tag):?>
    <span>
        <a href="<?php echo Yii::app()->createUrl('/search?category[]='.CHtml::encode($tag->category->alias));?>"><?php echo CHtml::encode($tag->category->title_name);?></a>
    </span>
    <?php endforeach;?>
</div>

<div class="social">
    <?php $this->widget('WidgetSocial');?>
</div>
<div class="clearfix"></div>

<hr>

<a name="list-comment"></a>
<h3>Комментарии:</h3>

<?php if(count($model->blogComments)>0):?>
<?php foreach($model->blogComments as $unit):?>
    <div class="comment-block">
      <div class="comment-avatar">
        <img src="" alt="">
      </div>
      <div class="comment-info">
        <div class="comment-info-description">
          <div class="comment-date">
              <?php echo CHtml::encode($unit->username);?>
              <i><?php echo CHtml::encode(Qwe::datef($unit->date_create, 'd.m.Y H:i:s'));?></i>
          </div>
          <div class="comment-text">
            <?php echo $unit->text_description?>
          </div>
        </div>
        <div class="comment-rating">
            <?php if(IpControl::model()->isAccess(IpControl::TYPE_COMM_BLOG, $unit->id)):?>
                <a href="<?php echo Yii::app()->createUrl('/blog/rating',array('type'=>IpControl::TYPE_COMM_BLOG,'id'=>$unit->id, 'status'=>'up'));?>"><i class="glyphicon glyphicon-arrow-up"></i></a>
                <a href="<?php echo Yii::app()->createUrl('/blog/rating',array('type'=>IpControl::TYPE_COMM_BLOG,'id'=>$unit->id, 'status'=>'down'));?>"><i class="glyphicon glyphicon-arrow-down"></i></a>
            <?php endif;?>
            <span><?php echo CHtml::encode($unit->rating);?></span>
        </div>
      </div>
    </div>
<?php endforeach;?>
<?php endif;?>

<a name="form-comment"></a>
<div class="row comment-write">
    <form action="#form-comment" method="POST">
        <div class="col-sm-8">
            <?php echo CHtml::activeTextArea($comment, 'text_description',array('placeholder'=>'Сообщение','class'=>'form-control comment-textarea'));?>
            <span class="error-block error-textarea">
                <?php echo CHtml::error($comment, 'text_description');?>
            </span>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
              <?php echo CHtml::activeTextField($comment, 'username', array('class'=>'form-control', 'placeholder'=>'Имя'));?>
              <span class="error-block error-input error-username">
                <?php echo CHtml::error($comment, 'username');?>
              </span>
          </div>
          <div class="form-group">
            <?php echo CHtml::activeTextField($comment, 'verifyCode', array('placeholder'=>'Проверочный код','class'=>'form-control'));?>
            <span class="error-block error-input error-verify">
                <?php echo CHtml::error($comment, 'verifyCode');?>
            </span>
            <?php $this->widget('CCaptcha',array('clickableImage'=>true,'showRefreshButton'=>false)); ?>
          </div>
        </div>
        <div class="col-sm-offset-10 blog-submit">
          <input type="submit" class="btn btn-primary" value="Отправить">
        </div>
    </form>
</div><!-- /.subscribe -->