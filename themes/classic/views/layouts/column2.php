<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="col-md-9">
    <?php $this->widget('zii.widgets.CBreadcrumbs', array('links'=>$this->breadcrumbs));?>
    
    <?php echo $content;?>

</div><!-- /.LEFT -->

<div class="col-sm-3  blog-sidebar">

  <?php $this->widget('WidgetSidebar');?>

</div><!-- /.blog-sidebar -->
<?php $this->endContent(); ?>